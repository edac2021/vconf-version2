package com.grp.vconf.entities;

import java.util.Set;
import java.util.stream.Collectors;  
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.grp.vconf.entities.Vehicle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity

@Table(name="models")
public class Model {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="model_id")
	private Long id;
	
	@Column(name="model_name",nullable = false)
	private String name ; 
	
	@ManyToOne
	@JsonManagedReference
	private Manufacturer manufacturer;
	
	@ManyToOne
	@JsonManagedReference
	private Segment segment;
	
	
	@Column(name="model_min_quantity",columnDefinition = "int default 1",nullable = false)
	private Integer minQuantity;
	
	@OneToMany(mappedBy="model",cascade=CascadeType.ALL) 
	@JsonManagedReference
	private Set<Vehicle> vehicle;
	
	private Long price ; 
	
	private String imageLink;
	
	
	/**
	 * boiler plate code for each entity like all args constructor no args constructor , getters setters to string
	 */
	
	public Model() {}
	
	public Model(Long id, String name, Manufacturer manufacturer, Segment segment, Integer minQuantity,
			Set<Vehicle> vehicle, Long price, String imageLink) {
		super();
		this.id = id;
		this.name = name;
		this.manufacturer = manufacturer;
		this.segment = segment;
		this.minQuantity = minQuantity;
		this.vehicle = vehicle;
		this.price = price;
		this.imageLink = imageLink;
	}






	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Segment getSegment() {
		return segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	public Integer getMinQuantity() {
		return minQuantity;
	}

	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}

	public Set<Vehicle> getVehicle() {
		return vehicle;
	}

	public void setVehicle(Set<Vehicle> vehicle) {
		this.vehicle = vehicle;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	@Override
	public String toString() {
		return "Model [id=" + id + ", name=" + name + ", manufacturer=" + manufacturer + ", segment=" + segment
				+ ", minQuantity=" + minQuantity + ", vehicle=" + vehicle + ", price=" + price + ", imageLink="
				+ imageLink + "]";
	}

	public void setTheTrain(Vehicle... vehicles) {
		for(Vehicle i : vehicles) {
			i.setModel(this);
		}
        this.vehicle = Stream.of(vehicles).collect(Collectors.toSet());
		
	}
	
}
