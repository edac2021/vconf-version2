package com.grp.vconf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.grp.vconf.dao.OrderRepository;
import com.grp.vconf.entities.Order;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class OrderRoutes {
	@Autowired
	OrderRepository orderRepository;
/**
 * 
 * @param order req body order
 * @return return saved instance
 */
	@PostMapping("/order")
	public Order addOrder(@RequestBody Order order) {
		return this.orderRepository.save(order);
	}

	/**
	 * 
	 * @param id return order based on email id
	 * @return list of orders from an id
	 */
	@GetMapping("/order/{id}")
	public List<Order> getAllOrders(@PathVariable String id) {
		return this.orderRepository.findByEmail(id);
	}
}
