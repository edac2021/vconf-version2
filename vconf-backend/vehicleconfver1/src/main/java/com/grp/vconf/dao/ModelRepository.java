package com.grp.vconf.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.grp.vconf.entities.Manufacturer;
import com.grp.vconf.entities.Model;

public interface ModelRepository extends JpaRepository<Model,Long>{
	@Query(value = "Select * from models m , segments s , manufacturers man "
			+ "where m.manufacturer_manufacturer_id = man.manufacturer_id and "
			+ "m.segment_segment_id = s.segment_id and  m.segment_segment_id=:segid"
			+ " and m.manufacturer_manufacturer_id =:manid", nativeQuery = true)
	List<Model> selectModelBasedOnManufacturerAndSegment(@Param("segid") Long segid , @Param("manid") Long manid);
}
