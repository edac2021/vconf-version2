package com.grp.vconf.Services;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.web.multipart.MultipartFile;

import com.grp.vconf.entities.FileDB;

public interface FileStorageService {
	public FileDB store(MultipartFile file,Long id) throws IOException;
	public FileDB getFile(String id);
	 public Stream<FileDB> getAllFiles() ;
}
