package com.grp.vconf.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.vconf.Services.ComponentService;
import com.grp.vconf.dao.ComponentRepository;
import com.grp.vconf.dao.UserRepository;
import com.grp.vconf.entities.Component;
import com.grp.vconf.entities.User;

@Service
public class ComponentServiceImpl implements ComponentService {
	@Autowired
	private ComponentRepository componentDao;
	

	@Override
	public List<Component> getAllComponents() {
		
		return componentDao.findAll();
	}

}
