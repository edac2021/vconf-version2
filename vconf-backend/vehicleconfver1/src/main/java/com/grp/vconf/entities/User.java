package com.grp.vconf.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;

import at.favre.lib.crypto.bcrypt.BCrypt;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity

@Table(name="users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="user_id")
	private Long id;
	@NotBlank(message = "user name cant be empty")
	@Email
	@Column(name="user_email",unique=true,nullable = false)
	private String email;
	/*
	 * 
    Minimum 6 and Maximum 20 Character.
    Atleast 1 number
    Atleast 1 alphabet in capitals.
    No Special char allowed

	 */
	//@Pattern(regexp = "^(?=.*\\d)(?=.*[A-Z]).{6,20}$")  
	@Column(name="user_password",nullable = false)
	private String password;
	@Column(name="user_role")
	private String role ;
	
	//company name -> had merged company and user table bcz of 1 to 1 mapping
	private String name;
	private String address;
	private String telephone;
	private String type;
	@Column(unique=true)
	private String gst;
	
	
	
	/**
	 * boiler plate code for each entity like all args constructor no args constructor , getters setters to string
	 */
	
	public User() {}
	public User(Long id, String email, String password, String role, String name, String address, String telephone,
			String type, String gst) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.role = role;
		this.name = name;
		this.address = address;
		this.telephone = telephone;
		this.type = type;
		this.gst = gst;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getGst() {
		return gst;
	}
	public void setGst(String gst) {
		this.gst = gst;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", password=" + password + ", role=" + role + ", name=" + name
				+ ", address=" + address + ", telephone=" + telephone + ", type=" + type + ", gst=" + gst + "]";
	}
	
	public boolean equals(User obj) {
		BCrypt.Result result = BCrypt.verifyer().verify(obj.getPassword().toCharArray(), this.getPassword());
		if(this.getEmail().equals(obj.getEmail()) && result.verified)
		return true;
		else return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
