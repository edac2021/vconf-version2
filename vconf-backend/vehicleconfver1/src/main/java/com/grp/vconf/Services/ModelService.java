package com.grp.vconf.Services;

import java.util.List;


import com.grp.vconf.entities.Component;
import com.grp.vconf.entities.Model;

public interface ModelService {
	public Model addModel(Model model);

	public Model getSingleModel(long id);

	public List<Model> getAllModel();

	public void deleteModel(long parseLong);
	public List<Model> getModelByManAndSeg(long segid, long manid);
	
}
