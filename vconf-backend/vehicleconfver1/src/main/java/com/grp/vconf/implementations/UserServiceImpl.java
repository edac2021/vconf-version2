package com.grp.vconf.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.vconf.Services.UserService;
import com.grp.vconf.dao.UserRepository;
import com.grp.vconf.entities.User;
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userDao;

	@Override
	public User addUser(User user) {
			return userDao.save(user);
	}


}
