package com.grp.vconf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.grp.vconf.Services.SegmentService;
import com.grp.vconf.entities.Segment;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class SegmentRoutes {
	@Autowired
	SegmentService segmentService; // done
		// get single
		@GetMapping("/segment/{id}")
		public Segment getSingleSegment(@PathVariable String id) {
			return this.segmentService.getSingleSegment(Long.parseLong(id));
		}

		// get all
		@GetMapping("/segment")
		public List<Segment> getAllSegment() {
			return this.segmentService.getAllSegment();
		}
}
