package com.grp.vconf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grp.vconf.entities.Component;
import com.grp.vconf.entities.Segment;


public interface SegmentRepository extends JpaRepository<Segment,Long>{

}
