package com.grp.vconf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grp.vconf.entities.AlternateComponent;

public interface AlternateComponentRepository extends JpaRepository<AlternateComponent, Long> {

}
