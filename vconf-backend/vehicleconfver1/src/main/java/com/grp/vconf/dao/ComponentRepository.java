package com.grp.vconf.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grp.vconf.entities.Component;


public interface ComponentRepository extends JpaRepository<Component,Long>{

}
