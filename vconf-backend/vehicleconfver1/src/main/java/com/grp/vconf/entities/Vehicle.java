package com.grp.vconf.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;




@Entity
@Table(name="vehicles")
public class Vehicle {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="vehicle_id")
	private Long id;
	
	
	
	@ManyToOne
	@JoinColumn(name="model_id")
	@JsonBackReference
	private Model model;
	
	
	
	@ManyToOne
	@JoinColumn(name="component_id")
	private Component component;
	
	
	
	@Column(name="component_type")
	private String ComponentType;
	@Column(name="vehicle_isconfigurable",columnDefinition = "boolean default false",nullable = false)
	private boolean configurable;
	
	/**
	 * boiler plate code for each entity like all args constructor no args constructor , getters setters to string
	 */
	
	public Vehicle(Long id, Model model, Component component, String componentType, boolean configurable) {
		super();
		this.id = id;
		this.model = model;
		this.component = component;
		ComponentType = componentType;
		this.configurable = configurable;
	}
	public Vehicle() {}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Model getModel() {
		return model;
	}
	public void setModel(Model model) {
		this.model = model;
	}
	public Component getComponent() {
		return component;
	}
	public void setComponent(Component component) {
		this.component = component;
	}
	public String getComponentType() {
		return ComponentType;
	}
	public void setComponentType(String componentType) {
		ComponentType = componentType;
	}
	public boolean isConfigurable() {
		return configurable;
	}
	public void setConfigurable(boolean configurable) {
		this.configurable = configurable;
	}
	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", model=" + model + ", component=" + component + ", ComponentType="
				+ ComponentType + ", configurable=" + configurable + "]";
	}
	
	
	
}
