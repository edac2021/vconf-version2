package com.grp.vconf.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.vconf.Services.ModelService;
import com.grp.vconf.dao.ComponentRepository;
import com.grp.vconf.dao.ModelRepository;
import com.grp.vconf.entities.Model;
@Service
public class ModelServiceImpl implements ModelService{
	@Autowired
	private ModelRepository modelDao;
	@Override
	public Model addModel(Model model) {
		// TODO Auto-generated method stub
		return modelDao.save(model);
	}

	@Override
	public Model getSingleModel(long id) {
		// TODO Auto-generated method stub
		return modelDao.findById(id).get();
	}

	@Override
	public List<Model> getAllModel() {
		// TODO Auto-generated method stub
		return modelDao.findAll();
	}

	@Override
	public void deleteModel(long parseLong) {
		// TODO Auto-generated method stub
		modelDao.deleteById(parseLong);
	}

	@Override
	public List<Model> getModelByManAndSeg(long segid, long manid) {
		// TODO Auto-generated method stub
		return modelDao.selectModelBasedOnManufacturerAndSegment(segid,manid);
	}


	
}
