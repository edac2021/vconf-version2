package com.grp.vconf.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grp.vconf.entities.Order;



public interface OrderRepository extends JpaRepository<Order,Long>{
	public List<Order> findByEmail(String email);
}
