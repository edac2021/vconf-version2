package com.grp.vconf.entities;

public class ResponseMessage {
	  private String message;
	  
	  
	  
	  /**
		 * boiler plate code for each entity like all args constructor no args constructor , getters setters to string
		 */

	  public ResponseMessage(String message) {
	    this.message = message;
	  }

	  public String getMessage() {
	    return message;
	  }

	  public void setMessage(String message) {
	    this.message = message;
	  }

	}