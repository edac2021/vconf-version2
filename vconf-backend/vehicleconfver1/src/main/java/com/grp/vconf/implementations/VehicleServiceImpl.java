package com.grp.vconf.implementations;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.vconf.Services.VehicleService;
import com.grp.vconf.dao.ComponentRepository;
import com.grp.vconf.dao.VehicleRepository;
import com.grp.vconf.entities.Vehicle;
@Service
public class VehicleServiceImpl implements VehicleService {
	@Autowired
	private VehicleRepository vehicleDao;
	@Override
	public Vehicle addVehicle(Vehicle vehicle) {
		
		return vehicleDao.save(vehicle);
	}

	@Override
	public Vehicle getSingleVehicle(long id) {
		
		return vehicleDao.findById(id).get();
	}

	@Override
	public List<Vehicle> getAllVehicles() {
		
		return vehicleDao.findAll();
	}

	@Override
	public void deleteVehicle(long parseLong) {
		
		vehicleDao.deleteById(parseLong);
	}

	@Override
	public List<Vehicle> getByModelId(Long id) {
		// TODO Auto-generated method stub
		return vehicleDao.findVehicleByModelId(id);
	}

}
