package com.grp.vconf.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity

@Table(name="components")
public class Component {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="component_id")
	private Long id;
	
	
	
	@Column(name="component_name",unique=true,nullable = false)
	private String name;
	
	
	@JsonBackReference
	@OneToMany(mappedBy="component")
	private Set<Vehicle> vehicle;
	
	@OneToMany(mappedBy="alternateComponent")
	@JsonManagedReference
	private Set<AlternateComponent> altComponent;
	
	
	
	/**
	 * boiler plate code for each entity like all args constructor no args constructor , getters setters to string
	 */
	
	
	
	public Component() {}
	public Component(Long id, String name, Set<Vehicle> vehicle, Set<AlternateComponent> altComponent) {
		super();
		this.id = id;
		this.name = name;
		this.vehicle = vehicle;
		this.altComponent = altComponent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Vehicle> getVehicle() {
		return vehicle;
	}

	public void setVehicle(Set<Vehicle> vehicle) {
		this.vehicle = vehicle;
	}

	public Set<AlternateComponent> getAltComponent() {
		return altComponent;
	}

	public void setAltComponent(Set<AlternateComponent> altComponent) {
		this.altComponent = altComponent;
	}

	@Override
	public String toString() {
		return "Component [id=" + id + ", name=" + name + ", vehicle=" + vehicle + ", altComponent=" + altComponent
				+ "]";
	}
	
	

}
