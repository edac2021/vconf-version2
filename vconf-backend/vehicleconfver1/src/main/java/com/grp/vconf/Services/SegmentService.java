package com.grp.vconf.Services;

import java.util.List;

import com.grp.vconf.entities.Component;
import com.grp.vconf.entities.Segment;
import com.grp.vconf.entities.User;

public interface SegmentService {
	public Segment addSegment(Segment segment);

	public Segment getSingleSegment(long id);

	public List<Segment> getAllSegment();

	public void deleteSegment(long id);
}
