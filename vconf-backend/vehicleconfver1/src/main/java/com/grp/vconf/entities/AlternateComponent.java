package com.grp.vconf.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity

public class AlternateComponent {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@OneToOne
	@JsonBackReference
	private Model model;
	@OneToOne
	@JsonManagedReference
	private Component component;
	@ManyToOne
	@JsonBackReference
	private Component alternateComponent;
	private Long price;
	
	/**
	 * boiler plate code for each entity like all args constructor no args constructor , getters setters to string
	 */
	
	
	public AlternateComponent() {}
	
	public AlternateComponent(Long id, Model model, Component component, Component alternateComponent, Long price) {
		super();
		this.id = id;
		this.model = model;
		this.component = component;
		this.alternateComponent = alternateComponent;
		this.price = price;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Model getModel() {
		return model;
	}
	public void setModel(Model model) {
		this.model = model;
	}
	public Component getComponent() {
		return component;
	}
	public void setComponent(Component component) {
		this.component = component;
	}
	public Component getAlternateComponent() {
		return alternateComponent;
	}
	public void setAlternateComponent(Component alternateComponent) {
		this.alternateComponent = alternateComponent;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "AlternateComponent [id=" + id + ", model=" + model + ", component=" + component
				+ ", alternateComponent=" + alternateComponent + ", price=" + price + "]";
	}
	
	
	
}
