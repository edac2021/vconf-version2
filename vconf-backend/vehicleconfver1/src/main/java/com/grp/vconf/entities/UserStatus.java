package com.grp.vconf.entities;


/**
 * 
 *  enum for login and reg
 *
 */
public enum UserStatus {
    SUCCESS_USER,
    SUCCESS_ADMIN,
    USER_ALREADY_EXISTS,
    FAILURE,COMPANY_ALREADY_EXISTS
}  