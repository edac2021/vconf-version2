package com.grp.vconf.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.vconf.Services.ManufacturerService;
import com.grp.vconf.dao.ComponentRepository;
import com.grp.vconf.dao.ManufacturerRepository;
import com.grp.vconf.entities.Manufacturer;
@Service
public class ManufacturerServiceImpl implements ManufacturerService{
	@Autowired
	private ManufacturerRepository manufacturerDao;
	

	@Override
	public Manufacturer getSingleManufacturer(long id) {
		// TODO Auto-generated method stub
		return manufacturerDao.findById(id).get();
	}

	@Override
	public List<Manufacturer> getAllManufacturer() {
		// TODO Auto-generated method stub
		return manufacturerDao.findAll();
	}

	

	@Override
	public List<Manufacturer> getManufacturerNameBySeg(long segid) {
		// TODO Auto-generated method stub
		return manufacturerDao.selectManufacturerbasedOnSegment(segid);
	}

}
