package com.grp.vconf.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.grp.vconf.entities.Manufacturer;

public interface ManufacturerRepository extends JpaRepository<Manufacturer,Long>{
	@Query(value = "Select distinct(man.manufacturer_id) ,man.manufacturer_name  from models m , segments s , manufacturers man "
			+ "where m.manufacturer_manufacturer_id = man.manufacturer_id and "
			+ "m.segment_segment_id = s.segment_id and  m.segment_segment_id=:segid", nativeQuery = true)
	List<Manufacturer> selectManufacturerbasedOnSegment(@Param("segid") Long segid);
}
