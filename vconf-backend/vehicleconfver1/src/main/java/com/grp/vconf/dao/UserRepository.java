package com.grp.vconf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.grp.vconf.entities.User;

public interface UserRepository extends JpaRepository<User,Long	> {
	public User findByEmail(String email);
	public User findByGst(String gst);
}
