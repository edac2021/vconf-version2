package com.grp.vconf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.grp.vconf.Services.ManufacturerService;
import com.grp.vconf.entities.Manufacturer;
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ManufacturerRoutes {
	@Autowired
	ManufacturerService manufacturerService; // done
	// get single
		@GetMapping("/manufacturer/{id}")
		public List<Manufacturer> getSingleManufacturerService(@PathVariable String id) {
			return this.manufacturerService.getManufacturerNameBySeg(Long.parseLong(id));
		}

		// get all
		@GetMapping("/manufacturer")
		public List<Manufacturer> getAllManufacturerService() {
			return this.manufacturerService.getAllManufacturer();
		}

}
