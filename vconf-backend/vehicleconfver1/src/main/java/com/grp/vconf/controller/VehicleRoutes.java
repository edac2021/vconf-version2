package com.grp.vconf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.grp.vconf.Services.VehicleService;
import com.grp.vconf.entities.Vehicle;


@RestController
@CrossOrigin(origins = "http://localhost:3000/")
public class VehicleRoutes {
	@Autowired
	VehicleService vehicleService;// done
	
	// get single
		@GetMapping("/vehicle/{id}")
		public List<Vehicle> getSingleVehicle(@PathVariable String id) {
			return this.vehicleService.getByModelId(Long.parseLong(id));
		}

		// get all
		@GetMapping("/vehicle")
		public List<Vehicle> getAllVehicle() {
			return this.vehicleService.getAllVehicles();
		}

}
