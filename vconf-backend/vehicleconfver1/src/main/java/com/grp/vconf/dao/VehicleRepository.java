package com.grp.vconf.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.grp.vconf.entities.User;
import com.grp.vconf.entities.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle,Long>{
	@Query(
			  value = "SELECT * FROM vehicles v WHERE v.model_id =:id", 
			  nativeQuery = true)
			List<Vehicle> findVehicleByModelId(@Param("id") Long id);
		
}
