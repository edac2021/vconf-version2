package com.grp.vconf.Services;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
	
	@Autowired
	JavaMailSender javaMailSender;


	public String sendEmailwithAttachment(File filePath) {
		try {

			MimeMessage message = javaMailSender.createMimeMessage();
			
			MimeMessageHelper messageHelper = 
					new MimeMessageHelper(message, true);
		
			messageHelper.setFrom("taksandedipratna44@gmail.com");
			messageHelper.setTo("dewanshgautam.rocky@gmail.com");
			messageHelper.setSubject("Vehicle Rental quotation");
			messageHelper.setText("Quotation",true);
			
			File file = filePath;
		
			messageHelper.addAttachment(file.getName(), file);
		
			javaMailSender.send(message);
			
		
			return "Mail sent successfully";
			
		} catch (Exception e) {
			e.printStackTrace();
			return "Mail sent failed";
		}
	}
}
