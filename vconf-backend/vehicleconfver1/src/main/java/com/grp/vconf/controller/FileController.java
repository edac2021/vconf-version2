package com.grp.vconf.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Blob;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.grp.vconf.Services.EmailService;
import com.grp.vconf.Services.FileStorageService;
import com.grp.vconf.entities.FileDB;
import com.grp.vconf.entities.ResponseFile;
import com.grp.vconf.entities.ResponseMessage;
@Controller
@CrossOrigin("http://localhost:3000")
public class FileController {
	 @Autowired
	  private FileStorageService storageService;
	  
	  @Autowired
	  EmailService emailService;
	  
	  
	  /**
	   * 
	   * @param file takes in a blob , to first store in db then make temporary copy of the same file and send it to user
	   * @param id user email id , to send him , mail
	   * @return response message -> custom
	   */
	  
	  @PostMapping("/mailupload")
	  public ResponseEntity<ResponseMessage> mailUploadedFile(@RequestParam("file") MultipartFile file ,@RequestParam("userId") String id) {
	    String message = "";
	    try {
	      FileDB storedFile = storageService.store(file,Long.parseLong(id));
	      storedFile.getId();
	      Blob blobPdf = new SerialBlob(storedFile.getData());
	      
	      File outputFile = new File("src\\main\\resources\\static\\"+storedFile.getId()+".pdf");
	      	try(FileOutputStream fout = new FileOutputStream(outputFile)){
		    
	      		IOUtils.copy(blobPdf.getBinaryStream(), fout);
	      	
	      	}	      
	      
	      
	      message = emailService.sendEmailwithAttachment(outputFile);
	      return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
	    } catch (Exception e) {
	      message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
	    }
	  }
}
