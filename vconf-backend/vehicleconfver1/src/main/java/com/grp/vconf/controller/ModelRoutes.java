package com.grp.vconf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.grp.vconf.Services.ModelService;
import com.grp.vconf.entities.Model;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ModelRoutes {
	@Autowired
	ModelService modelService; // done
	
	/*
	 * mapping for model
	 */

	@GetMapping("/model/{segid}/{manid}")
	public List<Model> getmodelbymanseg(@PathVariable("segid") String segid, @PathVariable("manid") String manid) {
		return this.modelService.getModelByManAndSeg(Long.parseLong(segid), Long.parseLong(manid));
	}

	// get single

	@GetMapping("/model/{id}")
	public Model getSingleModel(@PathVariable String id) {
		return this.modelService.getSingleModel(Long.parseLong(id));
	}

	// get all
	@GetMapping("/model")
	public List<Model> getAllModel() {
		return this.modelService.getAllModel();
	}
}
