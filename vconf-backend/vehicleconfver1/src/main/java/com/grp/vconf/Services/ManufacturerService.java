package com.grp.vconf.Services;

import java.util.List;

import com.grp.vconf.entities.Manufacturer;

public interface ManufacturerService {
	

	public Manufacturer getSingleManufacturer(long id);

	public List<Manufacturer> getAllManufacturer();

	public List<Manufacturer> getManufacturerNameBySeg(long segid); 
}
