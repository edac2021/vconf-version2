package com.grp.vconf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grp.vconf.Services.ComponentService;
import com.grp.vconf.entities.Component;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ComponentRoutes {
	@Autowired
	ComponentService componentService;
	
	// get all
		@GetMapping("/component")
		public List<Component> getSingleComponent() {
			return this.componentService.getAllComponents();
		}
}
