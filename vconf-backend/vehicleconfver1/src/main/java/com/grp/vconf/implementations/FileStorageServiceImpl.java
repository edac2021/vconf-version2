package com.grp.vconf.implementations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.util.StringUtils;
import com.grp.vconf.Services.FileStorageService;
import com.grp.vconf.dao.FileDBRepository;
import com.grp.vconf.entities.FileDB;

@Service
public class FileStorageServiceImpl implements FileStorageService{

	  @Autowired
	  private FileDBRepository fileDBRepository;

	@Override
	public FileDB store(MultipartFile file,Long id) throws IOException {
		 String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		 System.out.println(file.getContentType());
		    FileDB FileDB = new FileDB(fileName, file.getContentType(), file.getBytes(),id);
		    
		    
		    
		    
		    return fileDBRepository.save(FileDB);
	}

	@Override
	public FileDB getFile(String id) {
		// TODO Auto-generated method stub
		 return fileDBRepository.findById(id).get();
	}

	@Override
	public Stream<FileDB> getAllFiles() {
		// TODO Auto-generated method stub
		return fileDBRepository.findAll().stream();
	}

}
