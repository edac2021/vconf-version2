package com.grp.vconf.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.vconf.Services.SegmentService;
import com.grp.vconf.dao.ComponentRepository;
import com.grp.vconf.dao.SegmentRepository;
import com.grp.vconf.dao.UserRepository;
import com.grp.vconf.entities.Component;
import com.grp.vconf.entities.Segment;
import com.grp.vconf.entities.User;

@Service
public class SegmentServiceImpl implements SegmentService {
	@Autowired
	private SegmentRepository segmentDao;
	

	@Override
	public Segment getSingleSegment(long id) {
		return segmentDao.findById(id).get();
	}

	@Override
	public List<Segment> getAllSegment() {
		return segmentDao.findAll();
	}


	@Override
	public void deleteSegment(long id) {
		segmentDao.deleteById(id);
		
	}


	@Override
	public Segment addSegment(Segment segment) {

		return segmentDao.save(segment);
	}


	
	
}
