package com.grp.vconf.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Entity

@Table(name="manufacturers")
public class Manufacturer {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="manufacturer_id")
	private Long id;
	@Column(name="manufacturer_name",nullable = false)
	private String name;
	/*
	 * @ManyToOne
	 * 
	 * @JsonBackReference private Segment segment;
	 */
	
	@JsonBackReference
	@OneToMany(mappedBy="manufacturer", cascade = CascadeType.ALL)
	private Set<Model>models;
	
	
	/**
	 * boiler plate code for each entity like all args constructor no args constructor , getters setters to string
	 */
	
	
	public Manufacturer() {}
	public Manufacturer(Long id, String name, Set<Model> models) {
		super();
		this.id = id;
		this.name = name;
		this.models = models;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<Model> getModels() {
		return models;
	}
	public void setModels(Set<Model> models) {
		this.models = models;
	}
	@Override
	public String toString() {
		return "Manufacturer [id=" + id + ", name=" + name + ", models=" + models + "]";
	}


	
	
	
	
}
