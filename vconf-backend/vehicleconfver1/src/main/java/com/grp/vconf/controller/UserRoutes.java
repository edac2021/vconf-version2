package com.grp.vconf.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import java.util.Set;
import org.slf4j.LoggerFactory;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.grp.vconf.dao.UserRepository;

import com.grp.vconf.entities.Component;

import com.grp.vconf.entities.Manufacturer;
import com.grp.vconf.entities.Model;
import com.grp.vconf.entities.User;
import com.grp.vconf.entities.UserStatus;

import at.favre.lib.crypto.bcrypt.BCrypt;

import com.google.gson.Gson;
import com.grp.vconf.Services.EmailService;
@RestController
@CrossOrigin(origins = "http://localhost:3000/")
public class UserRoutes {
	
	/**
	 * logger for logging 
	 */
	  Logger logger=LoggerFactory.getLogger(UserRoutes.class);
	
	  
	  @Autowired
	UserRepository userRepository;
	
	
	@Autowired
	EmailService emailService;
	
	@GetMapping("/user/test")
	public String userTester() {
		return "user";
	}
	
	@GetMapping("/user")
	public List<User> getAllUsers() {
		return this.userRepository.findAll();
	} 



	@CrossOrigin(origins = "http://localhost:3000/")
	@PostMapping("/uploadfile")
	public String uploadFile(@RequestBody String str){
		
		byte[] decodedBytes = Base64.getDecoder().decode(str);
		try {
			FileUtils.writeByteArrayToFile(new File("C:\\Users\\Dewansh\\Desktop\\test.pdf"), decodedBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "great sucess";
		
	}
	
	
	@CrossOrigin()
	@PostMapping("/register")
	public UserStatus registerUser(@Valid @RequestBody User user) {
		
		
		User dbUser = userRepository.findByEmail(user.getEmail());
		if(dbUser!=null) {
			return UserStatus.USER_ALREADY_EXISTS;
		}
		 dbUser = userRepository.findByGst(user.getGst());
		if(dbUser!=null) {
			return UserStatus.COMPANY_ALREADY_EXISTS;
		}
		
		try {
			user.setPassword(BCrypt.withDefaults().hashToString(12, user.getPassword().toCharArray()));
		User u = userRepository.save(user);
		return getRole(u);
		}catch (Exception e) {
			return UserStatus.FAILURE;
		}
		
	}
	

	/**
	 * 	
	 * @param newUser takes in user to determine his role
	 * @return type of user for frontend to handle
	 */
	public UserStatus getRole(User newUser) {
		if(newUser.getRole().equals("ROLE_USER"))
	       	 return UserStatus.SUCCESS_USER;
	       	else
	       		return UserStatus.SUCCESS_ADMIN;
		
		
	}
	
	/**
	 * 
	 * @param user rest api input as User Object
	 * @return returns string , type of user or error message
	 */
	
    @CrossOrigin()
    @PostMapping("/login")
    public UserStatus loginUser(@Valid @RequestBody User user) {
        List<User> users = userRepository.findAll();
        User dbUser = userRepository.findByEmail(user.getEmail());
        
        if(dbUser!=null && dbUser.equals(user) )
        {logger.info("Login Sucessfully");
        	return getRole(dbUser);
           
         }
        logger.error("Login Failed");

        return UserStatus.FAILURE;
    }
 
    /**
     * 
     * @param id method to fet user based on email id
     * @return returns user
     */
    @GetMapping("/user/{id}")
	public User getSingleComponent(@PathVariable String id) {
		return this.userRepository.findByEmail(id);
	}
   
    /**
     * 
     * @param id email to detemine user
     * @param user request body
     * @return updated user
     */
    
    @PutMapping("/user/{id}")
	public User updateUser(@PathVariable String id, @RequestBody User user) {
    	 User dbUser =this.userRepository.findByEmail(id);
    	 dbUser.setAddress(user.getAddress());
    	 dbUser.setName(dbUser.getName());
    	 dbUser.setTelephone(user.getTelephone());
    	 dbUser.setType(user.getType());
    	 System.out.println(dbUser);
		return userRepository.save(dbUser);
	}
    
    /**
     * 
     * @param id deletes a user based on email id
     * @return void
     */
    @DeleteMapping("/user/{id}")
	public String deleteUser(@PathVariable String id) {
    	User dbUser =this.userRepository.findByEmail(id);
    	System.out.println(dbUser);
    	if(dbUser!=null) {
    		 this.userRepository.delete(dbUser);
    		
    		}
		return id;
	}
    
}
