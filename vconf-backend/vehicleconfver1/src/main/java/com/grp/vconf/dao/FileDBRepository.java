package com.grp.vconf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.grp.vconf.entities.FileDB;

@Repository
public interface FileDBRepository extends JpaRepository<FileDB, String> {

}