package com.grp.vconf.Services;

import java.util.List;
import java.util.Set;

import com.grp.vconf.entities.Vehicle;

public interface VehicleService {
	public Vehicle addVehicle(Vehicle component);

	public Vehicle getSingleVehicle(long id);

	public List<Vehicle> getAllVehicles();

	public void deleteVehicle(long parseLong);
	public List<Vehicle> getByModelId(Long id);
}
