package com.grp.vconf;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

//import com.grp.vconf.dao.CompanyRepository;
import com.grp.vconf.dao.ComponentRepository;
import com.grp.vconf.dao.ManufacturerRepository;
import com.grp.vconf.dao.ModelRepository;
import com.grp.vconf.dao.SegmentRepository;
//import com.grp.vconf.entities.Company;
import com.grp.vconf.entities.Component;
import com.grp.vconf.entities.Manufacturer;
import com.grp.vconf.entities.Model;
import com.grp.vconf.entities.Segment;

@SpringBootTest
class Vehicleconfver1ApplicationTests {

	
	//@Autowired
	//private CompanyRepository companyrepo;
	
	/*
	 * @Test public void testcreateCompany() { Company com=new Company();
	 * com.setId(1L); com.setName("M.D.Conveyor PVT.LTD"); com.setAddress("Pune");
	 * com.setType("PVT.LTD"); com.setGst("ABCD2312356");
	 * com.setTelephone("8975790487"); companyrepo.save(com); assert
	 * (companyrepo.findById(1L).get()) != null; }
	 */
	@Autowired
	ComponentRepository componentrepo;
	
	@Test
	public void Componenttestcreate() {
		Component comp=new  Component();
		comp.setId(2L);
		comp.setName("MusicSytem");
       
		 componentrepo.save(comp);
		assert ( componentrepo.findById(2L).get()) != null;
	}
	
	@Autowired
	ManufacturerRepository manufacturerrepo;
	@Test
	public void Manufacturertestcreate() {
		Manufacturer man=new  Manufacturer();
		man.setId(3L);
		man.setName("MarutiSuzuki");
		
		manufacturerrepo.save(man);
		assert ( componentrepo.findById(3L).get()) != null;
	}
	
	
	@Autowired
	 SegmentRepository segmentrepo;
	@Test
	public void Segmenttestcreate() {
		Segment seg=new  Segment();
		seg.setId(4L);
		seg.setName("SmallCar");
		
		segmentrepo.save(seg);
		assert ( segmentrepo.findById(4L).get()) != null;
	}
	
	@Autowired
	ModelRepository modelrepo;
	@Test
	public void Modeltestcreate() {
		Model mod=new  Model();
		mod.setId(5L);
		mod.setName("ALTo");
		mod.setPrice(200000l);
		mod.setMinQuantity(2);
		modelrepo.save(mod);
		assert ( modelrepo.findById(5L).get()) != null;
	}
	/*
	 * @Test public void testReadCompany() {
	 * 
	 * List<Company> list=companyrepo.findAll();
	 * assertThat(list).size().isGreaterThan(0); List<Component>
	 * list2=componentrepo.findAll(); assertThat(list2).size().isGreaterThan(0);
	 * List<Manufacturer> list3=manufacturerrepo.findAll();
	 * assertThat(list3).size().isGreaterThan(0); List<Segment>
	 * list4=segmentrepo.findAll(); assertThat(list4).size().isGreaterThan(0);
	 * List<Model> list5= modelrepo.findAll();
	 * assertThat(list5).size().isGreaterThan(0); }
	 */
	
	
	
	
	}