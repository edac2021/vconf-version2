import React, { Component } from "react";
import AsyncSelect from "react-select/async";
import Select from "react-select";
import Button from "react-bootstrap/Button";
import Swal from "sweetalert2";

class Welcome2 extends Component {
  constructor(props, context) {
    super(props, context);
    
    this.state = {
      selectedSegment: {},
      segmentOptions: [],
      selectedManufacturer: {},
      manufacturerOptions: [],
      selectedModel: {},
      modelOptions: [],
      modelMinQuantity: 1,
      minQuantities: {},
      minQuantityFetched : 1,
  
    };


    localStorage.removeItem("sum")
    
    var archive = [],
      keys = Object.keys(sessionStorage),
      i = 0,
      key;
      

    for (; (key = keys[i]); i++) {
      if (!Number.isNaN(Number.parseInt(key))) {
        sessionStorage.removeItem(key);
      }
    }
  }
  
  fetchSegments = (inputValue, callback) => {
    fetch("http://localhost:8080/segment", {
      method: "GET",
    })
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        const tempArray = [];
        if (data) {
          if (data.length) {
            data.forEach((element) => {
              tempArray.push({
                label: `${element.name}`,
                value: element.id,
              });
            });
          } else {
            tempArray.push({
              label: `${data.name}`,
              value: data.id,
            });
          }
        }
        callback(tempArray);
      })
      .catch((error) => {
        console.log(error, "catch the hoop");
      });
  };

  fetchManufacturers = (inputValue) => {
    fetch("http://localhost:8080/manufacturer/" + inputValue, {
      method: "GET",
    })
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        const tempArray = [];
        if (data) {
          if (data.length) {
            data.forEach((element) => {
              tempArray.push({
                label: `${element.name}`,
                value: element.id,
              });
            });
          } else {
            tempArray.push({
              label: `${data.name}`,
              value: data.id,
            });
          }
        }
        this.setState({ manufacturerOptions: tempArray }, () =>
          console.log(this.state.manufacturerOptions)
        );
      })
      .catch((error) => {
        console.log(error, "catch the hoop");
      });
  };

  fetchModels = (seg, man) => {
    fetch("http://localhost:8080/model/" + seg + "/" + man, {
      method: "GET",
    })
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        const tempArray = [];
        const minQuantity = [];
        if (data) {
          if (data.length) {
            data.forEach((element) => {
              minQuantity.push({
                label: `${element.id}`,
                value: element.minQuantity,
              });
              tempArray.push({
                label: `${element.name}`,
                value: element.id,
              });
            });
          } else {
            minQuantity.push({ label: `${data.id}`, value: data.minQuantity });
            tempArray.push({
              label: `${data.name}`,
              value: data.id,
            });
          }
        }
        this.setState(
          { modeltOptions: tempArray, minQuantities: minQuantity },
          () => {
            console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            console.log(this.state.modeltOptions);
            console.log(this.state.minQuantities);
          }
        );
      })
      .catch((error) => {
        console.log(error, "catch the hoop");
      });
  };

  onSegmentChange = (selectedSegment) => {
    if (selectedSegment) {
      this.setState(
        {
          selectedSegment,
        },
        () => {
          console.log(selectedSegment);
          this.fetchManufacturers(selectedSegment.value);
        }
      );
    }
  };

  onManufacturerChange = (selectedManufacturer) => {
    if (selectedManufacturer) {
      this.setState(
        {
          selectedManufacturer,
        },
        () => {
          console.log(selectedManufacturer);
          this.fetchModels(
            this.state.selectedSegment.value,
            selectedManufacturer.value
          );
        }
      );
    }
  };

  onModelChange = (selectedModel) => {
    if (selectedModel) {
      this.setState(
        {
          selectedModel,
        },
        () => {
          console.log(selectedModel);
          console.log("=============");
          let selectedModelvalue = selectedModel.value;
          let minQuantities = this.state.minQuantities;
          for (let i = 0; i < minQuantities.length; i++) {
            if (minQuantities[i].label == selectedModel.value) {
              
              this.setState({minQuantityFetched: minQuantities[i].value})
              this.setState({ minQuantity: minQuantities[i].value }, () =>
                console.log(this.state.minQuantity)
              );
            }
          }
        }
      );
    }
  };

  handleSegmentChange = (segmentOptions) => {
    this.setState({ segmentOptions });
  };
  handleManufacturerChange = (manufacturerOptions) => {
    this.setState({ manufacturerOptions });
  };
  handleMmodelChange = (modelOptions) => {
    this.setState({ modelOptions });
  };
  handleSubmit = () => {
    console.log(this.state);
    localStorage.setItem("minQuantity", this.state.minQuantity);
    if (this.state.selectedModel.value && this.state.minQuantityFetched<=this.state.minQuantity)
      window.location =
        "http://localhost:3000/defaultconfiguration/?id=" +
        this.state.selectedModel.value;
    else {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "You forgot to select Model or entered quantity less than min quantity",
      });
    }
  };
  onChangeQuantity = (e) => {
    this.setState({ minQuantity: e.target.value });
  };
  render() {
    return (
      <div>
        <section>
          <h1 className="heading">
            {" "}
            Select<span>Model</span>{" "}
          </h1>
        </section>

        <div style={{ marginLeft: "40%", width: "20%", marginLeft: "40%" }}>
          <div>
            <p>Segment Select</p>
            <AsyncSelect
              focus
              value={this.state.selectedSegment}
              loadOptions={this.fetchSegments}
              onChange={(e) => {
                this.onSegmentChange(e);
              }}
              defaultOptions={true}
            />
          </div>
          <div>
            <p>Manufacturer Select</p>
            <Select
              value={this.state.selectedManufacturer}
              options={this.state.manufacturerOptions}
              onChange={(e) => {
                this.onManufacturerChange(e);
              }}
              defaultOptions={true}
            />
          </div>
          <div>
            <p>Model Select</p>
            <Select
              value={this.state.selectedModel}
              options={this.state.modeltOptions}
              onChange={(e) => {
                this.onModelChange(e);
              }}
              defaultOptions={true}
            />
          </div>
          <br />
          <br />

          <form class="form-inline">
            <label class="sr-only" for="inlineFormInputName2">
              Min Quantity
            </label>
            <input
              type="number"
              class="form-control mb-2 mr-sm-2"
              id="inlineFormInputName2"
              placeholder={this.state.minQuantity}
              onChange={this.onChangeQuantity}
            />
            <Button variant="primary" onClick={this.handleSubmit}>
              Lets Go{" "}
            </Button>
          </form>
        </div>

        <br />
        <br />
      </div>
    );
  }
}
export default Welcome2;
