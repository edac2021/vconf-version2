import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

export default function MainAlternateSelector(props) {
  const { products, onAdd, featureName } = props;
  return (
    <main>
      <h2>{featureName}</h2>
      <div>
        <select className="form-select  h-100 w-100" data-width="100%">
          {products.map((product) => (
            <option
              onClick={() => {
                onAdd(product);
              }}
              key={product.id}
            >
              {" "}
              {product.component.name} @ ${product.price}{" "}
            </option>
          ))}
        </select>
      </div>
    </main>
  );
}
