import React from "react";

export default function Basket(props) {
  const { cartItems, onAdd, onRemove, featureId, featureName, onUpdate } =
    props;
  const itemsPrice = cartItems.reduce((a, c) => a + c.qty * c.price, 0);
  const taxPrice = itemsPrice * 0.28;
  const shippingPrice = itemsPrice > 10000 ? 0 : itemsPrice * 0.1;
  const totalPrice = itemsPrice + taxPrice + shippingPrice;
  console.log("feature id");
  console.log(featureId);
  console.log("featurename");
  console.log(featureName);

  let replacement = {
    featureId: featureId,
    featureName: featureName,
    replacementId: cartItems[0]?.id,
    replacementName: cartItems[0]?.component.name,
    price: totalPrice,
  };
  console.log("replacement");
  console.log(replacement);
  sessionStorage.setItem(featureId, JSON.stringify(replacement));
  // localStorage.setItem(featureId , JSON.stringify(replacement))

  var archive = [],
    //keys = Object.keys(localStorage)
    keys = Object.keys(sessionStorage),
    i = 0,
    key,
    sum = 0;

  for (; (key = keys[i]); i++) {
    if (!Number.isNaN(Number.parseInt(key))) {
      var item = JSON.parse(sessionStorage.getItem(key));
      archive.push(item.price);
    }
  }
  console.log(archive);
  for (let num of archive) {
    console.log("sum");
    sum = sum + num;
  }
  console.log(sum);
  //localStorage.setItem( 'sum', sum.toFixed(2) );
  localStorage.setItem("sum", sum.toFixed(2));
  onUpdate(sum);

  return (
    <aside>
      {" "}
      <h2>Customize selection</h2>
      {cartItems.length === 0 && <div>Cart is empty</div>}
      {cartItems.map((item) => (
        <div class="container" key={item.id}>
          <div class="row">
            <div class="col-6">{item.component.name}</div>

            <div class="col-3">
              <button
                class="btn btn-danger btn-sm"
                onClick={() => onRemove(item)}
              >
                -
              </button>{" "}
              <button
                class="btn btn-success btn-sm"
                onClick={() => onAdd(item)}
              >
                +
              </button>
            </div>

            <div class="col-3">
              {item.qty} x ${item.price.toFixed(2)}
            </div>
          </div>
        </div>
      ))}
      {cartItems.length !== 0 && (
        <>
          <hr></hr>
          <div class="container">
            <div class="row">
              <div class="col">Items Price</div>
              <div class="col">${itemsPrice.toFixed(2)}</div>
            </div>
            <div class="row">
              <div class="col">Tax Price @28%</div>
              <div class="col">${taxPrice.toFixed(2)}</div>
            </div>
            <div class="row">
              <div class="col">Shipping Price</div>
              <div class="col">${shippingPrice.toFixed(2)}</div>
            </div>

            <div class="row">
              <div class="col">
                <strong>Total Price</strong>
              </div>
              <div class="col">
                <strong>${totalPrice.toFixed(2)}</strong>
                {}
              </div>
            </div>
          </div>
          <hr />
        </>
      )}
    </aside>
  );
}
