import React, { Component } from "react";
import "./css/FooterStyles.css";
import { GrGoogle, GrFacebook, GrTwitter } from "react-icons/gr";
import { IconContext } from "react-icons";
function Footer() {
  return (
    <footer class="page-footer font-small stylish-color-dark pt-4">
      <hr />
      <div class="container text-center text-md-left">
        <div class="row">
          <div class="col-md-4 mx-auto">
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4">
              Vehicle-Rental
            </h5>
            <p>
              Our vision is to become the leading brand in the rental of all
              sorts of vehicles and to have the widest network of branches in
              GCC.
            </p>
          </div>

          <hr class="clearfix w-100 d-md-none" />

          <div class="col-md-2 mx-auto">
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4">
              our branches
            </h5>

            <ul class="list-unstyled">
              <li>
                <a href="#!">india</a>
              </li>
              <li>
                <a href="#!">USA</a>
              </li>
              <li>
                <a href="#!">russia</a>
              </li>
            </ul>
          </div>

          <hr class="clearfix w-100 d-md-none" />

          <div class="col-md-2 mx-auto">
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4">
              our services
            </h5>

            <ul class="list-unstyled">
              <li>
                <a href="#!">car rental</a>
              </li>
              <li>
                <a href="#!">Loans</a>
              </li>
              <li>
                <a href="#!">vehicle marketing</a>
              </li>
              <li>
                <a href="#!">Compliance</a>
              </li>
            </ul>
          </div>

          <hr class="clearfix w-100 d-md-none" />
        </div>
      </div>

      <hr />

      <ul class="list-unstyled list-inline text-center">
        <IconContext.Provider value={{ color: "#342f32", size: "2em" }}>
          <li class="list-inline-item">
            <a href="https://www.google.com/">
              <GrGoogle></GrGoogle>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="https://www.facebook.com/">
              <GrFacebook></GrFacebook>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="https://www.twitter.com/">
              <GrTwitter></GrTwitter>
            </a>
          </li>
        </IconContext.Provider>
      </ul>

      <div class="footer-copyright text-center py-3">
        © 2021 Copyright:
        <a href="https://mdbootstrap.com/">GROUP 19 E-DAC</a>
      </div>
    </footer>
  );
}

export default Footer;
