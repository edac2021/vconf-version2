import { React, useState } from "react";
import jsPDF from "jspdf";
import "jspdf-autotable";
import Swal from 'sweetalert2'

import { Alert } from "react-bootstrap";
import UserComponnet from "./UserComponnet";

export default function Confirm(props) {
  const minQuantity = localStorage.getItem("minQuantity");
  const retrieved = localStorage.getItem("fullModel");
  const model = JSON.parse(retrieved);
  var archive = [],
    //keys = Object.keys(localStorage)
    keys = Object.keys(sessionStorage),
    i = 0,
    key,
    sum = 0;
  let grandTotal = localStorage.getItem("sum");
  grandTotal=grandTotal??0;
  for (; (key = keys[i]); i++) {
    if (!Number.isNaN(Number.parseInt(key))) {
      var item = JSON.parse(sessionStorage.getItem(key));
      archive.push(item);
    }
  }
  console.log(archive);
  let componentArrayMailer = [];

  for (let num of archive) {
    for (let mod of model.vehicle) {
      if (mod.id === num.featureId && num.hasOwnProperty("replacementName")) {
        mod.component.name = num.replacementName + "[customised]";
      }
    }
  }
  console.log(sum);
  //localStorage.setItem( 'sum', sum.toFixed(2) );

  const print = () => {
    // initialize jsPDF
    const doc = new jsPDF();

    const tableColumn1 = ["Field", "Description"];
    // define an empty array of rows
    const tableRows1 = [];
    tableRows1.push(["segment ", model.segment.name]);
    tableRows1.push(["manufacturer ", model.manufacturer.name]);
    tableRows1.push(["Model Name  ", model.name]);
    doc.autoTable(tableColumn1, tableRows1, { startY: 20 });

    let user = JSON.parse(localStorage.getItem("userDetails"));

    doc.text("Vehicle rental", 14, 15);
    const tableColumn4 = ["Field", "Buyer details"];
    // define an empty array of rows
    const tableRows4 = [];
    tableRows4.push(["Company name  ", user.name]);
    tableRows4.push(["email  ", user.email]);
    tableRows4.push(["contact address  ", user.address]);
    tableRows4.push(["telephone ", user.telephone]);
    tableRows4.push(["type  ", user.type]);
    tableRows4.push(["gst  ", user.gst]);

    doc.autoTable(tableColumn4, tableRows4);

    // define the columns we want and their titles
    const tableColumn = ["Id", "Component name"];
    // define an empty array of rows
    const tableRows = [];

    // for each ticket pass all its data into an array

    let counter = 1;
    model.vehicle.forEach((ticket) => {
      const ticketData = [
        "Id" + counter,
        ticket.component.name,

        // called date-fns to format the date on the ticket
      ];
      counter = counter + 1;
      // push each tickcet's info into a row
      tableRows.push(ticketData);
    });

    // startY is basically margin-top
    doc.autoTable(tableColumn, tableRows);

    // we use a date string to generate our filename.

    const tableColumn3 = ["Field", "Price"];
    // define an empty array of rows
    const tableRows3 = [];
    tableRows3.push(["base price per model", model.price]);
    tableRows3.push(["Modicfication price", parseFloat(grandTotal)]);
    tableRows3.push([
      "total price",
      parseFloat(model.price) + parseFloat(grandTotal),
    ]);
    tableRows3.push(["quantity ", parseFloat(minQuantity)]);
    tableRows3.push([
      "quantity ",
      (parseFloat(model.price) + parseFloat(grandTotal)) *
        parseFloat(minQuantity),
    ]);
    doc.autoTable(tableColumn3, tableRows3);

    // ticket title. and margin-top + margin-left

    // we define the name of our PDF file.
    doc.save(model.name);

 
  };
  const goToWelcome = () => {
    window.location = "http://localhost:3000/welcome";
  };

  const orderTheItem = () => {
    const min = 1;
    const max = 100;
    const rand = min + Math.random() * (max - min);
    let obj = {
      id: rand,
      modelName: model.name,
      price:
        (parseFloat(model.price) + parseFloat(grandTotal)) *
        parseFloat(minQuantity),
      email: JSON.parse(localStorage.getItem("userDetails")).email,
    };

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Buy it!'
    }).then((result) => {
      if (result.isConfirmed) {
        fetch("http://localhost:8080/order/", {
          method: "POST",
          headers: { "Content-type": "application/json" },
          body: JSON.stringify(obj),
        }).then((r) => {
          console.log(r.json());
          Swal.fire(
            'Bought!',
            'Your order has been placed . you can go to profile to see details.',
            'success'
          )
        });

       
      }
    })

   
  };
  const sendMail = () => {
   
    // initialize jsPDF
    const doc = new jsPDF();

    const tableColumn1 = ["Field", "Description"];
    // define an empty array of rows
    const tableRows1 = [];
    tableRows1.push(["segment ", model.segment.name]);
    tableRows1.push(["manufacturer ", model.manufacturer.name]);
    tableRows1.push(["Model Name  ", model.name]);
    doc.autoTable(tableColumn1, tableRows1, { startY: 20 });

    let user = JSON.parse(localStorage.getItem("userDetails"));

    doc.text("Vehicle rental", 14, 15);
    const tableColumn4 = ["Field", "Buyer details"];
    // define an empty array of rows
    const tableRows4 = [];
    tableRows4.push(["Company name  ", user.name]);
    tableRows4.push(["email  ", user.email]);
    tableRows4.push(["contact address  ", user.address]);
    tableRows4.push(["telephone ", user.telephone]);
    tableRows4.push(["type  ", user.type]);
    tableRows4.push(["gst  ", user.gst]);

    doc.autoTable(tableColumn4, tableRows4);

    // define the columns we want and their titles
    const tableColumn = ["Id", "Component name"];
    // define an empty array of rows
    const tableRows = [];

    // for each ticket pass all its data into an array

    let counter = 1;
    model.vehicle.forEach((ticket) => {
      const ticketData = [
        "Id" + counter,
        ticket.component.name,

        // called date-fns to format the date on the ticket
      ];
      counter = counter + 1;
      // push each tickcet's info into a row
      tableRows.push(ticketData);
    });

    // startY is basically margin-top
    doc.autoTable(tableColumn, tableRows);

    // we use a date string to generate our filename.

    const tableColumn3 = ["Field", "Price"];
    // define an empty array of rows
    const tableRows3 = [];
    tableRows3.push(["base price per model", model.price]);
    tableRows3.push(["Modicfication price", parseFloat(grandTotal)]);
    tableRows3.push([
      "total price",
      parseFloat(model.price) + parseFloat(grandTotal),
    ]);
    tableRows3.push(["quantity ", parseFloat(minQuantity)]);
    tableRows3.push([
      "quantity ",
      (parseFloat(model.price) + parseFloat(grandTotal)) *
        parseFloat(minQuantity),
    ]);
    doc.autoTable(tableColumn3, tableRows3);

    // ticket title. and margin-top + margin-left

    // we define the name of our PDF file.
    var savedPdf = doc.output("blob");
    console.log("tostring saved pdf");

    console.log(savedPdf);
    // TRUE
    var jsonSaved = { file: savedPdf.toString() };

   
    let formData = new FormData();
    formData.append("file", savedPdf);
    formData.append(
      "userId",
      JSON.parse(localStorage.getItem("userDetails")).id
    );
    setShowSend(true);
    fetch("http://localhost:8080/mailupload", {
      body: formData,
      mode: "cors",
      method: "post",
      headers: new Headers({
        type: "application/pdf", // <-- Specifying the Content-Type
        "Access-Control-Allow-Origin": "*",
      }),
    })
      .then((response) => response.text())
      .then((responseText) => {
        setShowSend(false);
        setSucess(true);
        setTimeout(myFunctionTOdismissSucess, 3000);
      })
      .catch((error) => {
        setShowSend(false);
        setFailure(true);
        setTimeout(myFunctionTOdismissFailure, 3000);
      });
  };

  const myFunctionTOdismissSucess = () => {
    setSucess(false);
  };
  const myFunctionTOdismissFailure = () => {
    setSucess(false);
  };

  const [showSend, setShowSend] = useState(false);
  const [showSucess, setSucess] = useState(false);
  const [showFailure, setFailure] = useState(false);

  return (
    <div>
      <section>
        <h1 className="heading">
          {" "}
          <span>Confirm</span> order{" "}
        </h1>
      </section>

      <div class="container">
        <div class="row">
          <div class="col">
            <h3>STANDARD_FEATURE</h3>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                </tr>
              </thead>
              <tbody>
                {}
                {model.vehicle.map((emp) =>
                  emp.componentType == "STANDARD_FEATURE" ? (
                    <tr key={emp.id}>
                      <td> {emp.component.id} </td>
                      <td>{emp.component.name}</td>
                      {/* {componentArrayMailer.push(emp.component.name)} */}
                    </tr>
                  ) : null
                )}
              </tbody>
            </table>
            <h3>INTERIOR_FEATURE</h3>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                </tr>
              </thead>
              <tbody>
                {}
                {model.vehicle.map((emp) =>
                  emp.componentType == "INTERIOR_FEATURE" ? (
                    <tr key={emp.id}>
                      <td> {emp.component.id} </td>
                      <td>{emp.component.name}</td>
                      {/* {componentArrayMailer.push(emp.component.name)} */}
                    </tr>
                  ) : null
                )}
              </tbody>
            </table>
            <h3>EXTERIOR_FEATURE</h3>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                </tr>
              </thead>
              <tbody>
                {}
                {model.vehicle.map((emp) =>
                  emp.componentType == "EXTERIOR_FEATURE" ? (
                    <tr key={emp.id}>
                      <td> {emp.component.id} </td>
                      <td>{emp.component.name}</td>
                      {/* {componentArrayMailer.push(emp.component.name)} */}
                    </tr>
                  ) : null
                )}
              </tbody>
            </table>
            <h3>ACCESSORIES</h3>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                </tr>
              </thead>
              <tbody>
                {}
                {model.vehicle.map((emp) =>
                  emp.componentType == "ACCESSORIES" ? (
                    <tr key={emp.id}>
                      <td> {emp.component.id} </td>
                      <td>{emp.component.name}</td>
                      {/* {componentArrayMailer.push(emp.component.name)} */}
                    </tr>
                  ) : null
                )}
              </tbody>
            </table>
          </div>
          <div class="col">
            <div class="row">
              <h4> Car Segment {model.segment.name}</h4>
            </div>
            <div class="row">
              <h4> Car manufacturer{model.manufacturer.name}</h4>
            </div>
            <div class="row">
              <h4> Car model{model.name}</h4>
            </div>
            <h2>Picture</h2>
            <img
              src={model.imageLink}
              class="rounded float"
              alt="..."
              width="70%"
              height="50%"
            />
            <div class="container">
              <div class="row">
                <div class="col-sm">
                  <h5>base price </h5>
                </div>
                <div class="col-sm">$ {model.price}</div>
              </div>
              <div class="row">
                <div class="col-sm">
                  <h5>modifications</h5>
                </div>
                <div class="col-sm">${grandTotal}</div>
              </div>
              <div class="row">
                <div class="col-sm">
                  <h5>Quantity</h5>
                </div>
                <div class="col-sm">{minQuantity} units</div>
              </div>
              <div class="row">
                <div class="col-sm">
                  <h5>Price per unit</h5>
                </div>
                <div class="col-sm">
                  $ {parseFloat(model.price) + parseFloat(grandTotal)}
                </div>
              </div>
              <div class="row">
                <div class="col-sm">
                  <h5>Grand total</h5>
                </div>
                <div class="col-sm">
                  <strong>
                    ${" "}
                    {(parseFloat(model.price) + parseFloat(grandTotal)) *
                      parseFloat(minQuantity)}
                  </strong>
                </div>
              </div>
              <div class="row">
                <div class="col-sm">
                  <h5>User Details</h5>
                </div>
                <div class="col-sm">
                  <UserComponnet />
                </div>
              </div>
            </div>
            <Alert variant="info" show={showSend} dismissible>
              <Alert.Heading>Please wait we are sending email</Alert.Heading>
              <p>
                Getting the recipies right . and sending mail as soon as
                possible
              </p>
            </Alert>
            <Alert variant="success" show={showSucess} dismissible>
              <Alert.Heading>Email sent</Alert.Heading>
              <p>Please check your email</p>
            </Alert>
            <Alert variant="danger" show={showFailure} dismissible>
              <Alert.Heading>Oops ! something went wrong</Alert.Heading>
              <p>We are working on the error</p>
            </Alert>
            <button type="button" class="btn btn-success" onClick={print}>
              Print Quotation
            </button>
            <button type="button" class="btn btn-success" onClick={sendMail}>
              Mail Quotation
            </button>
            <button
              type="button"
              class="btn btn-success"
              onClick={orderTheItem}
            >
              order
            </button>
            <button type="button" class="btn btn-danger" onClick={goToWelcome}>
              Cancel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
