import React from "react";
import Swal from "sweetalert2";
import { NavLink } from "react-router-dom";
export class Employeedel extends React.Component {
  constructor(props) {
    super(props);
    this.state = { employee: {} };
  }
  divStyle = {
    margin: "40px",
    border: "5px solid pink",
  };
  async componentWillMount() {
    console.log(this.props.match.params.code);
    const response = await fetch(
      "http://localhost:8080/user/" + this.props.match.params.code
    );
    const res = await response.json();
    this.setState({ employee: res });
  }
  onDelEmployee = async (e) => {
    Swal.fire({
      title: "This is irreversible step . Still want to delete user",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Yes",
      denyButtonText: `NO`,
      confirmButtonColor: "#df4759",
      denyButtonColor: "#42ba96",
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        fetch("http://localhost:8080/user/" + this.props.match.params.code, {
          method: "Delete",
        }).then((res) => Swal.fire("Deleted User!", "", "success"));
      } else if (result.isDenied) {
        Swal.fire("User was not deleted", "", "info");
      }
    });
  };

  render() {
    return (
      <div>
        <section className="contact" id="contact">
          <h1 className="heading">
            {" "}
            <span>Delete</span> User{" "}
          </h1>
        </section>
        {console.log(this.state.employee)}

        <div class="container">
          <div class="row">
            <div class="col">Employee ID</div>
            <div class="col">
              <input
                class="form-control"
                type="text"
                name="id"
                ref="id"
                defaultValue={this.state.employee.id}
                disabled
              ></input>
            </div>
          </div>
          <div class="row">
            <div class="col">Employee email</div>
            <div class="col">
              <input
                class="form-control"
                type="text"
                name="email"
                ref="email"
                defaultValue={this.state.employee.email}
                disabled
              ></input>
            </div>
          </div>
          <div class="row">
            <div class="col">Employee role</div>
            <div class="col">
              <input
                class="form-control"
                type="text"
                name="role"
                ref="role"
                defaultValue={this.state.employee.role}
                disabled
              ></input>
            </div>
          </div>
          <div class="row">
            <div class="col">Employee company name</div>
            <div class="col">
              <input
                class="form-control"
                type="text"
                ref="name"
                name="name"
                defaultValue={this.state.employee.name}
                disabled
              ></input>
            </div>
          </div>
          <div class="row">
            <div class="col">Employee address</div>
            <div class="col">
              <input
                class="form-control"
                type="text"
                ref="address"
                name="address"
                defaultValue={this.state.employee.address}
                disabled
              ></input>{" "}
            </div>
          </div>
          <div class="row">
            <div class="col">Employee telephone</div>
            <div class="col">
              <input
                class="form-control"
                type="text"
                ref="telephone"
                name="telephone"
                defaultValue={this.state.employee.telephone}
                disabled
              ></input>
            </div>
          </div>

          <div class="row">
            <div class="col">Employee type:</div>
            <div class="col">
              <input
                class="form-control"
                type="text"
                ref="type"
                name="type"
                defaultValue={this.state.employee.type}
                disabled
              ></input>{" "}
            </div>
          </div>
          <div class="row">
            <div class="col">gst</div>
            <div class="col">
              <input
                class="form-control"
                type="text"
                ref="gst"
                name="gst"
                defaultValue={this.state.employee.gst}
                disabled
              ></input>
            </div>
          </div>
          <button class="btn btn-danger" onClick={this.onDelEmployee}>
            Delete
          </button>

          <NavLink className="nav-link" to="/admin">
            <button type="button" class="btn btn-info">
              back to admin page
            </button>
          </NavLink>
        </div>
      </div>
    );
  }
}
export default Employeedel;
