import React from "react";
import "./css/HomeStyles.css";
import { Container, Row, Col } from "react-bootstrap";
import { FaAward, FaSmileBeam, FaCheck } from "react-icons/fa";
import { IconContext } from "react-icons";
import Lottie from "react-lottie";
import animationData from "./Lottie/lf30_editor_yzoayu1c.json";
function About() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <>
      <section className="about" id="about">
        <Container fluid="md">
          <Row>
            <Col>
              <br />
              <br />
              <br />
              <br />
              <Lottie options={defaultOptions} width={400} />
            </Col>

            <Col>
              <div className="content">
                <h1 className="heading">
                  {" "}
                  <span>about</span> us{" "}
                </h1>
                <h6 className="title">big projects start with big dreams</h6>
                <p class="h6">
                  Our vision is to become the leading brand in the rental of all
                  sorts of vehicles and to have the widest network of branches
                  in GCC. It would be our earnest endeavour to continuously
                  improve our products and services by means of innovation,
                  following up customer feedback and self assessment.
                </p>
                <button type="button" class="btn btn-warning">
                  {" "}
                  Learn More
                </button>

                <IconContext.Provider value={{ color: "#342f32", size: "2em" }}>
                  <div className="icons-container">
                    <div className="icons">
                      <FaAward></FaAward>
                      <h6>14 award won</h6>
                    </div>
                    <div className="icons">
                      <FaSmileBeam></FaSmileBeam>
                      <h6>250 satisfied clients</h6>
                    </div>
                    <div className="icons">
                      <FaCheck></FaCheck>
                      <h6>200 projects completed</h6>
                    </div>
                  </div>
                </IconContext.Provider>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
}

export default About;
