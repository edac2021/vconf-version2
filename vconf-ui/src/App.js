
import "./App.css";
import Welcome2 from "./Welcome2";
import Home from "./Home";
import MyNavbar from "./MyNavbar";
import Footer from "./Footer";
import { Switch, Route } from "react-router-dom";
import About from "./About";
import Review from "./Review";
import Services from "./Services";
import Contact from "./Contact";
import DefaultConfiguration from "./DefaultConfiguration";
import Register from "./Register";
import Login from "./Login";
import Confirm from "./Confirm";
import AdminPanel from "./AdminPanel";
import UserDashboard from "./UserDashboard";
import { Employeeup } from "./Employeeup";
import Employeedel from "./Employeedel";
function App() {
  return (
    <div className="App">
      <MyNavbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/review" component={Review} />
        <Route path="/welcome" component={Welcome2} />
        <Route path="/services" component={Services} />
        <Route path="/contact" component={Contact} />
        <Route path="/defaultconfiguration" component={DefaultConfiguration} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/confirm" component={Confirm} />
        <Route path="/admin" component={AdminPanel} />
        <Route path="/logout" component={Login} />
        <Route path="/userprofile" component={UserDashboard} />


        {/* code to update the user on profile page of user */}
        <Route
          path="/employee/:code"
          render={(props) => (
            <Employeeup {...props} key={props.match.params.code} />
          )}
        />


        {/* code to delete the user on admin page  */}
        <Route
          path="/employeedel/:code"
          render={(props) => (
            <Employeedel {...props} key={props.match.params.code} />
          )}
        ></Route>
         {/* default route  */}
        <Route component={Home}/>
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
