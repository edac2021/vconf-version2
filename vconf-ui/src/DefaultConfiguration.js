import React, { Component } from "react";
import Feature from "./Feature";
export default class DefaultConfiguration extends Component {
  constructor() {
    super();
    this.state = {
      fullResult: null,
      gotData: false,
      vehicle: [],
      link: "",
      wantChange: false,
    };
    this.onCancel = this.onCancel.bind(this);
  }

  ToggleButton() {
    this.setState(() => ({
      wantChange: !this.state.wantChange,
    }),()=>{localStorage.removeItem("sum")});

  }

  componentDidMount() {
    const queryParams = new URLSearchParams(window.location.search);
    const id = queryParams.get("id");
    if (isNaN(id)) throw Error("error");
    fetch("http://localhost:8080/model/" + id)
      .then((res) => res.json())
      .then((result) => {
        console.log("model");
        console.log(result);

        this.setState({
          fullResult: result,
          gotData: true,
          vehicle: result.vehicle,
          link: result.imageLink,
        });

        localStorage.setItem("fullModel", JSON.stringify(result));
      })
      .catch((err) => console.log(err));
  }
  onCancel(event) {
    window.location = "http://localhost:3000/welcome";
  }

  render() {
    return (
      <section>
        <br />
        <br />
        <h1 className="heading">
          {" "}
          <span>Default</span>Configuration{" "}
        </h1>
        <div class="container">
          <div class="row">
            <div class="col">
              <h3>STANDARD_FEATURE</h3>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                  </tr>
                </thead>
                <tbody>
                  {}
                  {this.state.vehicle?.map((emp) =>
                    emp.componentType == "STANDARD_FEATURE" ? (
                      <tr key={emp.id}>
                        <td> {emp.component.id} </td>
                        <td>{emp.component.name}</td>
                      </tr>
                    ) : null
                  )}
                </tbody>
              </table>
              <h3>INTERIOR_FEATURE</h3>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                  </tr>
                </thead>
                <tbody>
                  {}
                  {this.state.vehicle?.map((emp) =>
                    emp.componentType == "INTERIOR_FEATURE" ? (
                      <tr key={emp.id}>
                        <td> {emp.component.id} </td>
                        <td>{emp.component.name}</td>
                      </tr>
                    ) : null
                  )}
                </tbody>
              </table>
              <h3>EXTERIOR_FEATURE</h3>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                  </tr>
                </thead>
                <tbody>
                  {}
                  {this.state.vehicle?.map((emp) =>
                    emp.componentType == "EXTERIOR_FEATURE" ? (
                      <tr key={emp.id}>
                        <td> {emp.component.id} </td>
                        <td>{emp.component.name}</td>
                      </tr>
                    ) : null
                  )}
                </tbody>
              </table>
              <h3>ACCESSORIES</h3>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                  </tr>
                </thead>
                <tbody>
                  {}
                  {this.state.vehicle?.map((emp) =>
                    emp.componentType == "ACCESSORIES" ? (
                      <tr key={emp.id}>
                        <td> {emp.component.id} </td>
                        <td>{emp.component.name}</td>
                      </tr>
                    ) : null
                  )}
                </tbody>
              </table>
            </div>
            <div class="col">
              <h2>Picture</h2>
              <img
                src={this.state.link}
                class="rounded float"
                alt="..."
                width="70%"
                height="50%"
              />
            </div>
          </div>
          <div
            class="btn-group"
            role="group"
            aria-label="Basic outlined example"
          >
            <button
              type="button"
              class="btn btn-outline-primary"
              onClick={() =>
                (window.location = "http://localhost:3000/confirm")
              }
            >
              Confirm
            </button>
            <button
              type="button"
              class="btn btn-outline-primary"
              onClick={() => this.ToggleButton()}
            >
              Configure
            </button>
            <button
              type="button"
              class="btn btn-outline-primary"
              onClick={() =>
                (window.location = "http://localhost:3000/welcome")
              }
            >
              cancel
            </button>
          </div>
        </div>

        {this.state.gotData === true && this.state.wantChange == true ? (
          <Feature data={this.state.fullResult} />
        ) : null}
      </section>
    );
  }
}
