import { Formik, Form } from "formik";
import React, { useEffect } from "react";
import * as Yup from "yup";
import { TextField } from "./TextField";
import Swal from "sweetalert2";
import { TiPencil } from "react-icons/ti";
import { IconContext } from "react-icons";
import {
  loadCaptchaEnginge,
  LoadCanvasTemplate,
  LoadCanvasTemplateNoReload,
  validateCaptcha,
} from "react-simple-captcha";
import { jsPDF } from "jspdf";

export const Register = () => {
  useEffect(() => {
    loadCaptchaEnginge(6);
  });

  const generateRegisterationForm = () => {
    const doc = new jsPDF();

    doc.text("Vehicle rental Registeration form", 14, 10);

    const tableColumn4 = ["Field", "User Details"];

    const tableRows4 = [];
    tableRows4.push(["Company Name  ", ""]);
    tableRows4.push(["Email  ", ""]);
    tableRows4.push(["contact address  ", ""]);
    tableRows4.push(["Contact Number ", ""]);
    tableRows4.push(["Type of Firm  ", ""]);
    tableRows4.push(["GST number  ", ""]);

    doc.autoTable(tableColumn4, tableRows4);

    doc.save("Registeration form vehicle rental.pdf");
  };

  let emailStore = "",
    passwordStore = "";
  const validate = Yup.object({
    // name :Yup.string().required(),
    // address :Yup.string().required(),
    // telephone :Yup.string().required().matches(new RegExp('[6-9]{2}[0-9]{6,8}'), 'Phone number is not valid'),
    // type :Yup.string().required(),
    // gst :Yup.string().required(),
    role: Yup.string()
      .oneOf(["ROLE_USER", "ROLE_ADMIN"])
      .required("role is required"),
    email: Yup.string().email("Email is invalid").required("Email is required"),
    password: Yup.string()
      .min(6, "Password must be at least 6 charaters")
      .required("Password is required"),
  });
  return (
    <Formik
      initialValues={{
        name: "",
        address: "",
        telephone: "",
        type: "",
        gst: "",
        email: "",
        password: "",
        role: "",
      }}
      validationSchema={validate}
      onSubmit={(values) => {
        console.log(values);
        emailStore = values.email;
        passwordStore = values.password;

        let user_captcha = document.getElementById("user_captcha_input").value;

        if (validateCaptcha(user_captcha) == true) {
          loadCaptchaEnginge(6);
        } else {
          Swal.fire({
            position: "center",
            icon: "error",
            title: "Wrong captcha",
            showConfirmButton: false,
            timer: 1000,
          });
          document.getElementById("user_captcha_input").value = "";

          return;
        }

        let demo = JSON.stringify(values);
        fetch("http://localhost:8080/register/", {
          method: "POST",
          headers: { "Content-type": "application/json" },
          body: demo,
        }).then((r) => {
          r.json().then((result) => {
            console.log(result);
            let userObject = {
              user: emailStore,
              password: passwordStore,
              role: "SUCCESS_USER",
            };
            if (result === "SUCCESS_USER") {
              sessionStorage.setItem("user", JSON.stringify(userObject));
              Swal.fire({
                position: "center",
                icon: "success",
                title: "Congratulations on your new account",
                showConfirmButton: true,
                confirmButtonText: "Next",
              }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                  window.location = "http://localhost:3000/welcome";
                }
              });
            } else if (result === "COMPANY_ALREADY_EXISTS") {
              Swal.fire({
                position: "center",
                icon: "info",
                title: "Company Already exist, U can go to login page",
                showConfirmButton: false,
                timer: 1500,
              });
            } else if (result === "SUCCESS_ADMIN") {
              userObject.role = "SUCCESS_ADMIN";
              sessionStorage.setItem("user", JSON.stringify(userObject));
              let timerInterval;
              Swal.fire({
                title: "Sucessful account creation",
                html: "please wait, we are build your dashboard",
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                  Swal.showLoading();
                  const b = Swal.getHtmlContainer().querySelector("b");
                  timerInterval = setInterval(() => {
                    b.textContent = Swal.getTimerLeft();
                  }, 500);
                },
                willClose: () => {
                  clearInterval(timerInterval);
                },
              }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                  window.location = "http://localhost:3000/admin";
                }
              });
            } else if (result === "USER_ALREADY_EXISTS") {
              Swal.fire({
                position: "center",
                icon: "info",
                title:
                  "User Already exist with this Email , U can go to login page",
                showConfirmButton: false,
                timer: 1500,
              });
            } else {
              Swal.fire({
                position: "center",
                icon: "error",
                title: "Failure",
                showConfirmButton: false,
                timer: 1500,
              });
            }
          });
        });
      }}
    >
      {(formik) => (
        <div style={{}}>
          <div class="container">
            <section>
              <h1 className="heading">
                {" "}
                <span>Sign Up</span>
                <IconContext.Provider
                  value={{ color: "#342f32", size: "3rem" }}
                >
                  <TiPencil></TiPencil>
                </IconContext.Provider>{" "}
              </h1>
            </section>
            <div class="row">
              <div class="col-sm">
                <h1> Online Registeration form </h1>
                <Form>
                  <TextField label="Company Name" name="name" type="text" />
                  <TextField
                    label="Company Address"
                    name="address"
                    type="text"
                  />
                  <TextField
                    label="Company Telephone"
                    name="telephone"
                    type="text"
                  />
                  <TextField label="Company Type" name="type" type="text" />
                  <TextField label="Company GST" name="gst" type="text" />
                  <TextField label="User Email" name="email" type="email" />
                  <TextField label="User Role" name="role" type="text" />
                  <TextField label="password" name="password" type="password" />
                  <LoadCanvasTemplate />
                  <TextField
                    type="text"
                    id="user_captcha_input"
                    name="user_captcha_input"
                    placeholder="Enter Captcha Value"
                    label="Captcha"
                  />

                  <button className="btn btn-success mt-3" type="submit">
                    Register
                  </button>
                  <button className="btn btn-danger mt-3 ml-3" type="reset">
                    Reset
                  </button>
                </Form>
              </div>
              <div class="col-sm">
                <h1> Offline Registeration form </h1>
                <button
                  type="button"
                  class="btn btn-primary"
                  onClick={generateRegisterationForm}
                >
                  Download form
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </Formik>
  );
};
export default Register;
