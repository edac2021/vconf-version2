import { React, useState } from "react";
import Table from "react-bootstrap/Table";


function UserComponnet() {
  let [id, setId] = useState("");

  let [cname, setCname] = useState("");
  let [address, setAddress] = useState("");
  let [Telephone, setTelephone] = useState("");
  let [type, setType] = useState("");
  let [gst, setGst] = useState("");
  let [email, setEmail] = useState("");
  let [role, setRole] = useState("");
  const sessionUser = sessionStorage.getItem("user");
  let user = JSON.parse(sessionUser);

  fetch("http://localhost:8080/user/" + user.user)
    .then((res) => res.json())
    .then((result) => {
      setId(result.id);
      setEmail(result.email);
      setCname(result.name);
      setAddress(result.address);
      setRole(result.role);
      setTelephone(result.telephone);
      setType(result.type);
      setGst(result.gst);
      localStorage.setItem("userDetails", JSON.stringify(result));
    })
    .catch((err) => console.log(err));

  return (
    <>
      <Table striped bordered hover variant="table table-success table-striped">
        <thead>
          <tr>
            <th>User Details</th>

            <th>Company Name</th>

            <th>Address</th>

            <th>Telephone</th>

            <th>Type</th>

            <th>GST</th>

            <th>Email</th>

            <th>Role</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>{id}</td>

            <td>{cname}</td>

            <td>{address}</td>

            <td>{Telephone}</td>

            <td>{type}</td>

            <td>{gst}</td>

            <td>{email}</td>

            <td>{role}</td>
          </tr>
        </tbody>
      </Table>
    </>
  );
}

export default UserComponnet;
