import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "./css/HomeStyles.css";
import banner from "./images/banner.jpg";
function Home() {
  return (
    <>
      <section className="home" id="home">
        <div class="container">
          <div class="row">
            <div class="col-lg">
              <div className="content">
                <h1 class>grow your business with us</h1>
                <p>
                  {" "}
                  Build the best product, cause no unnecessary harm, use
                  business to inspire and implement solutions to the
                  environmental crisis.
                </p>
                <button type="button" class="btn btn-warning">
                  {" "}
                  Lets Get Start with Us....
                </button>
              </div>
            </div>
            <div class="col-lg"> 
              <div className="image">
                <img
                  src={banner}
                  alt="Fast car image"
                  className="img-fluid max-width: 100% height: auto shadow-lg"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Home;
