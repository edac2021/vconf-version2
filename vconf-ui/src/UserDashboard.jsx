import React, { useState } from "react";
import UserComponnet from "./UserComponnet";
import { NavLink } from "react-router-dom";
export default function UserDashboard() {
  let [orders, setOrders] = useState([]);
  const fetchUsers = () => {
    fetch(
      "http://localhost:8080/order/" +
        JSON.parse(localStorage.getItem("userDetails")).email
    )
      .then((res) => res.json())
      .then((result) => {
        console.log(result);

        setOrders(result);
      })
      .catch((err) => console.log(err));
  };

  return (
    <div>
      <div class="container">
        <section className="contact" id="contact">
          <h1 className="heading">
            {" "}
            <span>User</span> Profile{" "}
          </h1>
        </section>
        <UserComponnet />
        <NavLink
          className="nav-link"
          to={
            "/employee/" + JSON.parse(localStorage.getItem("userDetails")).email
          }
        >
          <button type="button" class="btn btn-info">
            Update profile
          </button>
        </NavLink>
        <button type="button" class="btn btn-info" onClick={fetchUsers}>
          fetch orders
        </button>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Order id</th>
              <th scope="col">Model Name</th>
              <th scope="col"> Price </th>
              <th scope="col">email</th>
            </tr>
          </thead>
          <tbody>
            {console.log(orders)}
            {orders.map((order) => {
              return (
                <tr>
                  {" "}
                  <td>{order.id}</td>
                  <td>{order.modelName}</td>
                  <td>$ {order.price}</td>
                  <td>{order.email} </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}
