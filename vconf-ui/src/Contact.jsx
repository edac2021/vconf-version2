import React from "react";
import Swal from "sweetalert2";

function Contact() {
  function sendMessage() {
    const name = document.getElementById("name").value;
    const email = document.getElementById("email").value;
    const number = document.getElementById("number").value;
    const message = document.getElementById("message").value;

    //  if (name===null || name===""){
    //   alert("Name can't be blank");
    //   return false;
    // }
    // else if(!email.match( /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/)){
    //   alert("enter valid mail");
    //   return false;
    //   }
    //    else if(!number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)){
    //     alert("enter valid number");
    //     return false;
    //   }else if(!message.match(/w+/)){
    //     alert("enter valid mesage");
    //     return false;
    //   }

    Swal.fire({
      icon: "success",
      title: "Message has been sent",
      showConfirmButton: false,
      timer: 1500,
    });
  }

  return (
    <>
      <br />

      <section className="contact" id="contact">
        <h1 className="heading">
          {" "}
          <span>Contact</span> us{" "}
        </h1>
        <div className="icons-container">
          <div className="icons">
            <i className="fas fa-phone"></i>
            <h3>our number</h3>
            <p>+9189-8342-5207</p>
            <p>+9198-5030-8623</p>
          </div>

          <div className="icons">
            <i className="fas fa-envelope"></i>
            <h3>our email</h3>
            <p>
              dipratna
              <wbr />
              taksande
              <wbr />
              @gmail.com
            </p>
            <p>
              lanjewar
              <wbr />
              gaurav07
              <wbr />
              @gmail.com
            </p>
          </div>
          <div className="icons">
            <i className="fas fa-map-marker-alt"></i>
            <h3>our location</h3>
            <p>HQ - mumbai, india - 400104</p>
            <p>Delhi, india - 110055</p>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <iframe
                className="map"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30153.788252261566!2d72.82321484621745!3d19.141690214227783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b63aceef0c69%3A0x2aa80cf2287dfa3b!2sJogeshwari%20West%2C%20Mumbai%2C%20Maharashtra%20400047!5e0!3m2!1sen!2sin!4v1629804296059!5m2!1sen!2sin"
                allowfullscreen=""
                loading="lazy"
              ></iframe>
            </div>
            <div class="col-sm">
              <form action="">
                <input
                  id="name"
                  type="text"
                  placeholder="name"
                  className="box"
                  required
                />
                <input
                  id="email"
                  type="email"
                  placeholder="email"
                  className="box"
                  required
                />
                <input
                  id="number"
                  type="number"
                  placeholder="number"
                  className="box"
                  required
                />
                <textarea
                  name=""
                  placeholder="message"
                  className="box"
                  id="message"
                  cols="30"
                  rows="1"
                  required
                ></textarea>
                <input
                  type="button"
                  onClick={() => {
                    sendMessage();
                  }}
                  value="send message"
                  className="btn btn-warning"
                />
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Contact;
