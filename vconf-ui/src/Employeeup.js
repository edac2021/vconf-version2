import React from "react";
import Swal from "sweetalert2";
import { NavLink } from "react-router-dom";
export class Employeeup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { employee: {} };
  }
  async componentWillMount() {
    console.log(this.props);

    const response = await fetch(
      "http://localhost:8080/user/" + this.props.match.params.code
    );
    const res = await response.json();
    this.setState({ employee: res });
  }

  onCreateEmployee = (e) => {
    let emp = this.state.employee;
    emp.name = this.refs.name.value;
    emp.address = this.refs.address.value;
    emp.type = this.refs.type.value;
    emp.telephone = this.refs.telephone.value;
    let demo = JSON.stringify(emp);
    console.log(emp);

    Swal.fire({
      title: "Do you want to save the changes?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Save",
      denyButtonText: `Don't save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        fetch("http://localhost:8080/user/" + this.props.match.params.code, {
          method: "PUT",
          headers: { "Content-type": "application/json" },
          body: demo,
        }).then((r) => {
          console.log(r.json());
          Swal.fire("Saved!", "", "success");
        });
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  render() {
    return (
      <div>
        <section className="contact" id="contact">
          <h1 className="heading">
            {" "}
            <span>Edit</span> Profile{" "}
          </h1>
        </section>
        <form>
          <p>
            <label>
              Employee ID :{" "}
              <input
                type="text"
                name="id"
                ref="id"
                defaultValue={this.state.employee.id}
                disabled
              ></input>
            </label>
          </p>
          <p>
            <label>
              Employee email :{" "}
              <input
                type="text"
                name="email"
                ref="email"
                defaultValue={this.state.employee.email}
                disabled
              ></input>
            </label>
          </p>
          <p>
            <label>
              Employee role :{" "}
              <input
                type="text"
                name="role"
                ref="role"
                defaultValue={this.state.employee.role}
                disabled
              ></input>
            </label>
          </p>
          <p>
            <label>
              Employee company name :{" "}
              <input
                type="text"
                ref="name"
                name="name"
                defaultValue={this.state.employee.name}
              ></input>
            </label>
          </p>
          <p>
            <label>
              Employee address:{" "}
              <input
                type="text"
                ref="address"
                name="address"
                defaultValue={this.state.employee.address}
              ></input>
            </label>
          </p>
          <p>
            <label>
              Employee telephone:{" "}
              <input
                type="text"
                ref="telephone"
                name="telephone"
                defaultValue={this.state.employee.telephone}
              ></input>
            </label>
          </p>
          <p>
            <label>
              Employee type:{" "}
              <input
                type="text"
                ref="type"
                name="type"
                defaultValue={this.state.employee.type}
              ></input>
            </label>
          </p>
          <p>
            <label>
              Employee gst:{" "}
              <input
                type="text"
                ref="gst"
                name="gst"
                defaultValue={this.state.employee.gst}
                disabled
              ></input>
            </label>
          </p>

          <button
            type="button"
            class="btn btn-primary"
            onClick={this.onCreateEmployee}
          >
            Update Profile
          </button>
        </form>

        <NavLink className="nav-link" to="/userprofile">
          <button type="button" class="btn btn-info">
            back to profile
          </button>
        </NavLink>
      </div>
    );
  }
}
