import { Component } from "react";
import Lottie from "react-lottie";
import animationData from "./Lottie/44656-error.json";



const ErrorComponent = () => {
  return (
    <div class="d-flex justify-content-center">
      <h1>Something went wrong</h1>
    </div>
  );
};

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};
export default class Errorboundary extends Component {
  state = {
    hasError: false,
    error: { message: "", stack: "" },
    info: { componentStack: "" },
  };

  static getDerivedStateFromError = (error) => {
    return { hasError: true };
  };

  componentDidCatch = (error, info) => {
    this.setState({ error, info });
  };

  render() {
    const { hasError, error, info } = this.state;
    console.log(error, info);
    const { children } = this.props;

    return hasError ? (
      <>
        <div class="container">
          <div class="row">
            <div class="col-sm">
              <ErrorComponent />
            </div>
          </div>
          <div class="row">
            <div class="col-sm">
              <Lottie options={defaultOptions} height={400} width={400} />
            </div>
          </div>
        </div>
      </>
    ) : (
      children
    );
  }
}
