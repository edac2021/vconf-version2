 const data = {
    id: 1,
    name: "Maruti Suzuki Baleno",
    manufacturer: {
      id: 1,
      name: "Maruti Suzuki"
    },
    segment: {
      id: 2,
      name: "Compact Car"
    },
    minQuantity: 2,
    vehicle: [
      {
        id: 4,
        component: {
          id: 4,
          name: "Independent Multi-Link Rear Suspension",
          altComponent: [
            {
              id: 3,
              component: {
                id: 13,
                name: "Lower Anchors and Tethers for Children (LATCH)",
                altComponent: []
              },
              price: 1123
            },
            {
              id: 5,
              component: {
                id: 15,
                name: "Emergency Trunk Opener",
                altComponent: []
              },
              price: 2345
            },
            {
              id: 4,
              component: {
                id: 14,
                name: "Child-Seat Tether Anchor (Rear-Center)",
                altComponent: []
              },
              price: 345
            }
          ]
        },
        configurable: false,
        componentType: "STANDARD_FEATURE"
      },
      {
        id: 15,
        component: {
          id: 15,
          name: "Emergency Trunk Opener",
          altComponent: []
        },
        configurable: false,
        componentType: "INTERIOR_FEATURE"
      },
      {
        id: 3,
        component: {
          id: 3,
          name: "Double Wishbone Front Suspension",
          altComponent: []
        },
        configurable: false,
        componentType: "STANDARD_FEATURE"
      },
      {
        id: 12,
        component: {
          id: 12,
          name: "Anti-Lock Braking System (ABS)",
          altComponent: []
        },
        configurable: false,
        componentType: "INTERIOR_FEATURE"
      },
      {
        id: 11,
        component: {
          id: 11,
          name: "Four-wheel Disc Brakes",
          altComponent: []
        },
        configurable: false,
        componentType: "EXTERIOR_FEATURE"
      },
      {
        id: 20,
        component: {
          id: 20,
          name: "Driver's Seat With 4Way Power Adjustment",
          altComponent: []
        },
        configurable: false,
        componentType: "ACCESSORIES"
      },
      {
        id: 7,
        component: {
          id: 7,
          name: "3-Point rear Seat Belts",
          altComponent: []
        },
        configurable: false,
        componentType: "EXTERIOR_FEATURE"
      },
      {
        id: 5,
        component: {
          id: 5,
          name: "Front and Rear Stabilizer Bars",
          altComponent: []
        },
        configurable: false,
        componentType: "STANDARD_FEATURE"
      },
      {
        id: 9,
        component: {
          id: 9,
          name: "Dual-Stage, Dual Threshold Front Airbags (SRS)",
          altComponent: []
        },
        configurable: false,
        componentType: "EXTERIOR_FEATURE"
      },
      {
        id: 13,
        component: {
          id: 13,
          name: "Lower Anchors and Tethers for Children (LATCH)",
          altComponent: []
        },
        configurable: false,
        componentType: "INTERIOR_FEATURE"
      },
      {
        id: 18,
        component: {
          id: 18,
          name: "Power Door Locks",
          altComponent: []
        },
        configurable: false,
        componentType: "ACCESSORIES"
      },
      {
        id: 19,
        component: {
          id: 19,
          name: "Cruise Control",
          altComponent: []
        },
        configurable: false,
        componentType: "ACCESSORIES"
      },
      {
        id: 1,
        component: {
          id: 1,
          name: "177-hp, 2.4-liter, 16-Valve, DOHC, i-VTEC®, 4-Cylinder Engine",
          altComponent: [
            {
              id: 1,
              component: {
                id: 3,
                name: "Double Wishbone Front Suspension",
                altComponent: []
              },
              price: 111111
            },
            {
              id: 2,
              component: {
                id: 12,
                name: "Anti-Lock Braking System (ABS)",
                altComponent: []
              },
              price: 222222
            }
          ]
        },
        configurable: false,
        componentType: "EXTERIOR_FEATURE"
      },
      {
        id: 16,
        component: {
          id: 16,
          name: "Blue star Air Conditioning with Air Filtration System",
          altComponent: [
            {
              id: 6,
              component: {
                id: 1,
                name: "177-hp, 2.4-liter, 16-Valve, DOHC, i-VTEC®, 4-Cylinder Engine",
                altComponent: [
                  {
                    id: 1,
                    component: {
                      id: 3,
                      name: "Double Wishbone Front Suspension",
                      altComponent: []
                    },
                    price: 111111
                  },
                  {
                    id: 2,
                    component: {
                      id: 12,
                      name: "Anti-Lock Braking System (ABS)",
                      altComponent: []
                    },
                    price: 222222
                  }
                ]
              },
              price: 2325
            },
            {
              id: 7,
              component: {
                id: 2,
                name: "5-Speed Manual Transmission",
                altComponent: []
              },
              price: 2363
            }
          ]
        },
        configurable: false,
        componentType: "INTERIOR_FEATURE"
      },
      {
        id: 2,
        component: {
          id: 2,
          name: "5-Speed Manual Transmission",
          altComponent: []
        },
        configurable: false,
        componentType: "STANDARD_FEATURE"
      },
      {
        id: 14,
        component: {
          id: 14,
          name: "Child-Seat Tether Anchor (Rear-Center)",
          altComponent: []
        },
        configurable: false,
        componentType: "INTERIOR_FEATURE"
      },
      {
        id: 17,
        component: {
          id: 17,
          name: "Power Windows with Auto-Up/Down Driver's and Front Passenger's windows",
          altComponent: []
        },
        configurable: false,
        componentType: "ACCESSORIES"
      },
      {
        id: 6,
        component: {
          id: 6,
          name: "Variable Gear Ratio (VGR) Power-Assisted Rack-and-Pinion Steering",
          altComponent: []
        },
        configurable: false,
        componentType: "EXTERIOR_FEATURE"
      },
      {
        id: 8,
        component: {
          id: 8,
          name: "Front 3-Point Seat Belts with Automatic Tensioning System",
          altComponent: []
        },
        configurable: false,
        componentType: "EXTERIOR_FEATURE"
      },
      {
        id: 10,
        component: {
          id: 10,
          name: "Advanced Compatibility Engineering™ (ACE™) body structure",
          altComponent: []
        },
        configurable: false,
        componentType: "EXTERIOR_FEATURE"
      }
    ],
    price: 200000,
    imageLink: "http://localhost:8080/zachary-edmundson-clIOdtfd_MY-unsplash.jpg"
  }
  export default data;