import React, { Component } from "react";
import { Accordion, Button } from "react-bootstrap";
export default class AdminPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      components: [],
      manufacturers: [],
      models: [],
      users: [],
    };
  }

  fetchComponents = () => {
    fetch("http://localhost:8080/component/")
      .then((res) => res.json())
      .then((result) => {
        console.log("components");
        this.setState({ components: result });
      })
      .catch((err) => console.log(err));
  };
  fetchManufacturers = () => {
    fetch("http://localhost:8080/manufacturer/")
      .then((res) => res.json())
      .then((result) => {
        console.log("components");
        this.setState({ manufacturers: result });
      })
      .catch((err) => console.log(err));
  };

  fetchModel = () => {
    fetch("http://localhost:8080/model/")
      .then((res) => res.json())
      .then((result) => {
        console.log("components");
        this.setState({ models: result });
      })
      .catch((err) => console.log(err));
  };
  fetchUsers = () => {
    fetch("http://localhost:8080/user/")
      .then((res) => res.json())
      .then((result) => {
        console.log("users");
        this.setState({ users: result });
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <div>
        return (
        <div class="container">
          <section className="contact" id="contact">
            <h1 className="heading">
              {" "}
              <span>Admin</span> Page{" "}
            </h1>
          </section>

          <Accordion>
          <Accordion.Item eventKey="3">
              <Accordion.Header>users</Accordion.Header>
              <Accordion.Body>
                <Button variant="primary" onClick={this.fetchUsers}>
                  Load users
                </Button>{" "}
                <table class="table table-hover">
                  <thead style={{ width: "40%" }}>
                    <tr>
                      <th scope="col">email</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.users.map((user) => {
                      return (
                        <tr key={user.id}>
                          <td>{user.email}</td>
                          {/* <td><a href={'/employee/'+model.email}>edit</a></td> */}
                          <td>
                             
                            <a
                              class="btn btn-danger"
                              href={"/employeedel/" + user.email}
                            >
                              delete
                            </a>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="0">
              <Accordion.Header>Components</Accordion.Header>
              <Accordion.Body>
                <Button variant="primary" onClick={this.fetchComponents}>
                  Load components
                </Button>{" "}
                <table class="table table-hover">
                  <thead style={{ width: "40%" }}>
                    <tr>
                      <th scope="col">Component name</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.components.map((component) => {
                      return (
                        <tr>
                          <td>{component.name}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="1">
              <Accordion.Header>Manufacturers</Accordion.Header>
              <Accordion.Body>
                <Button variant="primary" onClick={this.fetchManufacturers}>
                  Load Manufacturer
                </Button>{" "}
                <table class="table table-hover">
                  <thead style={{ width: "40%" }}>
                    <tr>
                      <th scope="col">Manufacturer name</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.manufacturers.map((manufacturer) => {
                      return (
                        <tr>
                          <td>{manufacturer.name}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </Accordion.Body>
            </Accordion.Item>

            <Accordion.Item eventKey="2">
              <Accordion.Header>models</Accordion.Header>
              <Accordion.Body>
                <Button variant="primary" onClick={this.fetchModel}>
                  Load Manufacturer
                </Button>{" "}
                <table class="table table-hover">
                  <thead style={{ width: "40%" }}>
                    <tr>
                      <th scope="col">models name</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.models.map((model) => {
                      return (
                        <tr>
                          <td>{model.name}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </Accordion.Body>
            </Accordion.Item>
           
          </Accordion>
        </div>
      </div>
    );
  }
}
