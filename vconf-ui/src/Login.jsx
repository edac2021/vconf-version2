import { Formik, Form } from "formik";
import React from "react";
import * as Yup from "yup";
import { TextField } from "./TextField";
import Swal from "sweetalert2";

const Login = () => {
  const validate = Yup.object({
    email: Yup.string()
      .max(50, "Must be 15 characters or less")
      .required("Required"),
    password: Yup.string()
      .min(6, "Password must be at least 6 charaters")
      .required("Password is required"),
  });
  let emailStore = "",
    passwordStore = "";
  sessionStorage.clear();

  return (
    <Formik
      initialValues={{
        email: "",
        password: "",
      }}
      validationSchema={validate}
      onSubmit={(values) => {
        console.log(values);
        emailStore = values.email;
        passwordStore = values.password;
        let demo = JSON.stringify(values);
        console.log(demo);
        fetch("http://localhost:8080/login", {
          method: "POST",
          mode: "cors",
          headers: { "Content-type": "application/json" },
          body: demo,
        }).then((r) => {
          r.json().then((result) => {
            console.log("result-login");
            console.log(result);
            let userObject = {
              user: emailStore,
              password: passwordStore,
              role: "SUCCESS_USER",
            };
            if (result === "SUCCESS_USER") {
              sessionStorage.setItem("user", JSON.stringify(userObject));
              window.location = "http://localhost:3000/welcome";
            } else if (result === "SUCCESS_ADMIN") {
              userObject.role = "SUCCESS_ADMIN";
              sessionStorage.setItem("user", JSON.stringify(userObject));
              window.location = "http://localhost:3000/admin";
            } else if (result === "FAILURE") {
              Swal.fire({
                position: "center",
                icon: "info",
                title: "Either password or email incorrect",
                showConfirmButton: false,
                timer: 1500,
              });
            }
          });
        });
      }}
    >
      {(formik) => (
        <section style={{ marginLeft: "25%", width: "50%" }}>
          <div>
            <div className="heading">
              {" "}
              <h1>
                {" "}
                <span>Login</span>{" "}
              </h1>
            </div>

            <Form>
              <TextField label="Email" name="email" type="text" />

              <TextField label="password" name="password" type="password" />

              <button className="btn btn-success mt-3" type="submit">
                Sign In
              </button>
            </Form>
          </div>
        </section>
      )}
    </Formik>
  );
};

export default Login;
