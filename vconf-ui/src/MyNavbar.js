import NavDropdown from "react-bootstrap/NavDropdown";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import { NavLink } from "react-router-dom";
import logo from "./images/vehicle.png";

export default function MyNavbar(props) {
  const removeUser = () => {
    sessionStorage.removeItem("user");
    window.location = "http://localhost:3000/";
  };

  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top">
        <Container>
          <NavLink className="none" to="/">
            <Navbar.Brand>
              <img
                alt=""
                src={logo}
                height="30"
                className="d-inline-block align-top"
              />{" "}
              Vehicle-Rental
            </Navbar.Brand>
          </NavLink>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto"></Nav>
            <Nav>
              {/* <NavLink className="nav-link" to='/'>Home</NavLink > */}
              {JSON.parse(sessionStorage.getItem("user")) === null ? (
                <NavLink className="nav-link" to="/">
                  Home
                </NavLink>
              ) : null}
              {sessionStorage.getItem("user") ? null : (
                <NavLink className="nav-link" to="/login">
                  Log In
                </NavLink>
              )}
              {sessionStorage.getItem("user") ? null : (
                <NavLink className="nav-link" to="/register">
                  Register
                </NavLink>
              )}
              {sessionStorage.getItem("user") ? null : (
                <NavLink className="nav-link" to="/services">
                  Services
                </NavLink>
              )}
              {sessionStorage.getItem("user") ? null : (
                <NavLink className="nav-link" to="/about">
                  About
                </NavLink>
              )}

              {sessionStorage.getItem("user") ? null : (
                <NavLink className="nav-link" to="/contact">
                  Contact
                </NavLink>
              )}
              {sessionStorage.getItem("user") ? null : (
                <NavLink className="nav-link" to="/review">
                  Review
                </NavLink>
              )}
              {JSON.parse(sessionStorage.getItem("user"))?.role ===
              "SUCCESS_USER" ? (
                <NavLink className="nav-link" to="/welcome">
                  welcome
                </NavLink>
              ) : null}
              {JSON.parse(sessionStorage.getItem("user"))?.role ===
              "SUCCESS_USER" ? (
                <NavLink className="nav-link" to="/userprofile">
                  Profile
                </NavLink>
              ) : null}
              {JSON.parse(sessionStorage.getItem("user"))?.role ===
              "SUCCESS_ADMIN" ? (
                <NavLink className="nav-link" to="/admin">
                  Admin Panel
                </NavLink>
              ) : null}
              {JSON.parse(sessionStorage.getItem("user")) !== null ? (
                <NavLink className="nav-link" to="/logout" onClick={removeUser}>
                  Logout
                </NavLink>
              ) : null}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
