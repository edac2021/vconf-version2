import React from "react";
import s2 from "./images/s2.png";
import s4 from "./images/s4.png";
import s6 from "./images/s6.png";
import p3 from "./images/p3.png";
import "./css/HomeStyles.css";
function Services() {
  return (
    <>
      <section className="services my-3" id="services">
        <section className="contact">
          <div className="heading">
            {" "}
            <h1>
              {" "}
              our <span>services</span>{" "}
            </h1>
          </div>
        </section>
        <div class="row">
          <div class="col-sm-3">
            <div className="box-container">
              <div className="box">
                <img src={s2} alt="" />
                <h3>car rental</h3>
                <p>
                  Affordable car hire in India. We find you the best deals by
                  partnering with major hire car companies without hidden costs.
                  Book car rentals in India.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div className="box-container">
              <div className="box">
                <img src={s4} alt="" />
                <h3>One click Loan</h3>
                <p>
                  Lets grow together. We understand the needs of business , so
                  here we present our special loan facility with all our orders
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div className="box-container">
              <div className="box">
                <img src={s6} alt="" />
                <h3>Compliance</h3>
                <p>
                  We help you comply with the law related to road and transport.
                  our dedicated team of lawyers work day and night , to have
                  safe business
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div className="box-container">
              <div className="box">
                <img src={p3} alt="" />
                <h3> Marketing</h3>
                <p>
                  A picture speaks thousand words and a video speaks then
                  thousand words. we have a team of creative marketing experts.
                  Who can increase you market presence
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Services;
