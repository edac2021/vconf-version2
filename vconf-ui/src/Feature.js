import React, { useState } from "react";
import EachFeature from "./EachFeature";
import { Accordion } from "react-bootstrap";
export default function Feature(props) {
  const data = props.data;
  const [count, setCount] = useState(0);
  const onUpdate = (value) => {
    setCount(value);
  };

  let custom0 = false;
  let custom1 = false;
  let custom2 = false;
  let custom3 = false;
  return (
    <main>
      <h1>GRAND TOTAL: {count.toFixed(2)}</h1>
      <h2>ALTERNATE</h2>

      <div class="container-fluid">
        <Accordion defaultActiveKey="0">
          <Accordion.Item eventKey="0">
            <Accordion.Header>STANDARD_FEATURE</Accordion.Header>
            <Accordion.Body>
              {data.vehicle.map((component) => {
                if (
                  component.component.altComponent.length !== 0 &&
                  component.componentType === "STANDARD_FEATURE"
                ) {
                  custom0 = true;
                  return (
                    <div class="row">
                      <EachFeature
                        data={component}
                        onUpdate={onUpdate}
                      ></EachFeature>
                    </div>
                  );
                }
              })}
              {custom0 === false ? <h4>no customization available</h4> : <></>}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="1">
            <Accordion.Header>INTERIOR_FEATURE</Accordion.Header>
            <Accordion.Body>
              {data.vehicle.map((component) => {
                if (
                  component.component.altComponent.length !== 0 &&
                  component.componentType === "INTERIOR_FEATURE"
                ) {
                  custom1 = true;
                  return (
                    <div class="row">
                      <EachFeature
                        data={component}
                        onUpdate={onUpdate}
                      ></EachFeature>
                    </div>
                  );
                }
              })}
              {custom1 === false ? <h4>no customization available</h4> : <></>}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="2">
            <Accordion.Header>EXTERIOR_FEATURE</Accordion.Header>
            <Accordion.Body>
              {data.vehicle.map((component) => {
                if (
                  component.component.altComponent.length !== 0 &&
                  component.componentType === "EXTERIOR_FEATURE"
                ) {
                  custom2 = true;
                  return (
                    <div class="row">
                      <EachFeature
                        data={component}
                        onUpdate={onUpdate}
                      ></EachFeature>
                    </div>
                  );
                }
              })}
              {custom2 === false ? <h4>no customization available</h4> : <></>}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="3">
            <Accordion.Header>ACCESSORIES</Accordion.Header>
            <Accordion.Body>
              {data.vehicle.map((component) => {
                if (
                  component.component.altComponent.length !== 0 &&
                  component.componentType === "ACCESSORIES"
                ) {
                  custom3 = true;
                  return (
                    <div class="row">
                      <EachFeature
                        data={component}
                        onUpdate={onUpdate}
                      ></EachFeature>
                    </div>
                  );
                }
              })}
              {custom3 === false ? <h4>no customization available</h4> : <></>}
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </div>
    </main>
  );
}
