import React from "react";
import pic1 from "./images/pic1.jpeg";
import pic2 from "./images/pic2.jpeg";
import pic3 from "./images/pic3.jpeg";
import "./css/HomeStyles.css";
function Review() {
  return (
    <div>
      <section className="review" id="review">
        <h3 className="heading">
          {" "}
          client's <span>review</span>{" "}
        </h3>
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div className="box-container">
                <div className="box">
                  <div className="user">
                    <img src={pic1} alt="" />
                    <div className="info">
                      <h3>john deo</h3>
                      <div className="stars">
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star-half-alt"></i>
                      </div>
                    </div>
                    <i className="fas fa-quote-right"></i>
                  </div>
                  <p>
                    All motor vehicles are likely to become blemished in one way
                    or another and for many reasons over time. Operating as a
                    repair shop, Vision Force is there to do their best at
                    reinvigorating your vehicle.
                  </p>
                  <p>
                    The mechanics here can have a look at your vehicle to
                    specify the quantity of work required, name the likely price
                    of services and provide additional consultation.
                  </p>{" "}
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div className="box-container">
                <div className="box">
                  <div className="user">
                    <img src={pic2} alt="" />
                    <div className="info">
                      <h3>john deo</h3>
                      <div className="stars">
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star-half-alt"></i>
                      </div>
                    </div>
                    <i className="fas fa-quote-right"></i>
                  </div>
                  <p>
                    Just wanted to say a quick thank you to Martin and his team
                    at Vision4accidents.co.uk for taking all the stress away and
                    dealing with the other persons insurance company when I had
                    a non-fault accident recently. I just gave them all the
                    details of the accident and because it wasn't my fault they
                    did everything on my behalf.{" "}
                  </p>
                  <p>
                    {" "}
                    Absolutely excellent service and I would advice anyone to
                    use them if they are unlucky enough to be involved in an
                    accident which is not their fault.
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div className="box-container">
                <div className="box">
                  <div className="user">
                    <img src={pic3} alt="" />
                    <div className="info">
                      <h3>john deo</h3>
                      <div className="stars">
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star"></i>
                        <i className="fas fa-star-half-alt"></i>
                      </div>
                    </div>
                    <i className="fas fa-quote-right"></i>
                  </div>
                  <p>
                    This is not only selling tons of cars you gain relationships
                    and knowledge. Everyone works hard and has fun doing it.
                  </p>
                  <p>
                    The owner cares and concerned with all his associates. The
                    diversity and culture is amazing and always growing.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Review;
