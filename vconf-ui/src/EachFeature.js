import MainAlternateSelector from "./MainAlternateSelector";
import Basket from "./Basket";

import { useState } from "react";
function EachFeature(props) {
  console.log(props);
  const { data, onUpdate } = props;
  console.log("in app");

  const { altComponent: products } = data.component;
  const { id: featureId, name: featureName } = data.component;
  console.log("featureName");
  console.log(featureName);
  const defaultOption = {
    id: Math.ceil(Math.random() * 10000000),
    name: data.name,
    price: 0,
  };
  console.log(products);
  let [cartItems, setCartItems] = useState([]);
  const onAdd = (product) => {
    const othersExist = cartItems.find((x) => x.id !== product.id);
    if (othersExist) {
      cartItems = cartItems.filter((item) => item.id === product.id);
      console.log("after removing shit");
      console.log(cartItems);
      setCartItems(cartItems);
    }

    const exist = cartItems.find((x) => x.id === product.id);
    if (exist) {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x
        )
      );
    } else {
      setCartItems([...cartItems, { ...product, qty: 1 }]);
    }
  };
  const onRemove = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist.qty === 1) {
      setCartItems(cartItems.filter((x) => x.id !== product.id));
    } else {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
        )
      );
    }
  };

  return (
    <div>
      <br />

      <div class="row">
        <div class="col-8">
          <MainAlternateSelector
            onAdd={onAdd}
            products={products}
            featureName={featureName}
          ></MainAlternateSelector>
        </div>
        <div class="col-4">
          <td>
            <Basket
              onAdd={onAdd}
              cartItems={cartItems}
              onRemove={onRemove}
              featureId={featureId}
              featureName={featureName}
              onUpdate={onUpdate}
            ></Basket>
          </td>
        </div>
      </div>
    </div>
  );
}

export default EachFeature;
